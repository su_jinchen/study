# Spring_WebSocket_STOMP

​	本文没有调试成功，请勿参考！

​	不是所有浏览器都支持websocket协议，为了使得Websocket的应用能够兼容那些不支持的浏览器，可以使用STOMP协议进行处理。其中SocketJs与STOMP都是针对浏览器不支持websocket的情况做的弥补。咱就不学了，再见！

> STOMP协议：https://stomp.github.io/stomp-specification-1.2.html

​	WebSocket 协议定义了两种消息类型：文本消息和二进制消息，但是它们的内容未定义。定义了一种机制，供 Client 端和服务器协商子协议(即高级消息传递协议)，以在 WebSocket 上使用以定义每个消息可以发送的消息种类，每个消息的格式和内容，等等。上。子协议的使用是可选的，但是无论哪种方式，Client 端和服务器都需要就定义消息内容的某种协议达成一致。

[	STOMP](https://stomp.github.io/stomp-specification-1.2.html#Abstract)是一种简单的面向文本的消息传递协议，最初是为 Ruby，Python 和 Perl 等脚本语言创建的，以连接到企业消息代理。它旨在解决常用消息传递模式的子集。 STOMP 可以在任何可靠的 2 路流网络协议(例如 TCP 和 WebSocket)上使用。尽管 STOMP 是面向文本的协议，但是消息的有效载荷可以是文本或二进制。

## 福利

与使用原始 WebSocket 相比，使用 STOMP 作为子协议使 Spring 框架和 Spring Security 可以提供更丰富的编程模型。关于 HTTP 与原始 TCP 的关系以及它如何使 Spring MVC 和其他 Web 框架提供丰富的功能，可以得出相同的观点。以下是好处列表：

> - 无需发明自定义消息协议和消息格式。
> - 可用的 STOMP Client 端在 Spring Framework 中包括[Java client](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/websocket.html#websocket-stomp-client)。
> - 消息代理(例如 RabbitMQ，ActiveMQ 和其他代理)可以(可选)用于 Management 订阅和 Broadcast 消息。
> - 可以使用任意数量的`@Controller`来组织应用程序逻辑，并根据 STOMP 目标 Headers 将消息路由到它们，而对于给定的连接，使用单个`WebSocketHandler`处理原始 WebSocket 消息。
> - 使用 Spring Security 基于 STOMP 目的地和消息类型来保护消息。

## 启用STOMP

​	直接使用WebSocket（或SockJS）就很类似于使用TCP套接字来编写Web应用。因为没有高层级的线路协议（wire protocol），因此就需要我们定义应用之间所发送消息的语义，还需要确保连接的两端都能遵循这些语义。STOMP在WebSocket之上提供了一个基于帧的线路格式（frame-based wire format）层，用来定义消息的语义。

​	乍看上去，STOMP的消息格式非常类似于HTTP请求的结构。与HTTP请求和响应类似，STOMP帧由命令、一个或多个头信息以及负载所组成。例如，如下就是发送数据的一个STOMP帧：

```SAS
SEND
destination:/app/marco
content-length:20

{\"message\":\"Marco!\"}
```

### 1.创建消息请求和响应体

```java
package com.xiaopangzi.stomp.message;

/**
 * 描述: 消息请求内容体
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
public class HellowMessage {
    /**
     * name:姓名
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

```

```java
package com.xiaopangzi.stomp.message;

/**
 * 描述: 消息响应内容体
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
public class HellowGreeting {
    /**
     * content:问候内容
     */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

```

### 2.创建一个消息处理控制器

```java
package com.xiaopangzi.stomp.controller;

import com.xiaopangzi.stomp.message.HellowGreeting;
import com.xiaopangzi.stomp.message.HellowMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

/**
 * 描述: 问候消息处理器
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
@Controller
public class GreetingController {

    @MessageMapping("/hellow")
    @SendTo("/topic/greetings")
    public HellowGreeting greeting(HellowMessage message) throws Exception {
        //模拟延迟
        Thread.sleep(1000);
        return new HellowGreeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
}

```

### 3.配置Spring以启用WebSocket和STOMP消息传递

```java
package com.xiaopangzi.stomp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    /**
     * 注册stomp的服务端点
     * @param registry
     */
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        /**
         * "/portfolio"是 WebSocket(或 SockJS)Client 端需要连接到的端点的 HTTP URL，以进行 WebSocket 握手。
         * 启用SockJS后备选项，以便在WebSocket不可用时可以使用备用传输。
         */
        registry.addEndpoint("/portfolio").withSockJS();
    }

    /**
     * 注册消息经纪人
     * @param registry
     */
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        /**
         * 其目标 Headers 以"/app"开头的 STOMP 消息将路由到@Controller类中的@MessageMapping方法。
         */
        registry.setApplicationDestinationPrefixes("/app");
        /**
         * 使用内置的消息代理进行订阅和广播；将目标 Headers 以“/topic”或“/queue”开头的消息路由到代理。
         */
        registry.enableSimpleBroker("/topic", "/queue");
    }
}

```

### 4.创建浏览器客户端

```html
<!DOCTYPE html>
<html>
<head>
    <title>Hello WebSocket</title>
    <link href="/webjars/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/main.css" rel="stylesheet">
    <script src="/webjars/jquery/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/stomp.min.js"></script>
    <script src="/app.js"></script>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
<div id="main-content" class="container">
    <div class="row">
        <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group">
                    <label for="connect">WebSocket connection:</label>
                    <button id="connect" class="btn btn-default" type="submit">Connect</button>
                    <button id="disconnect" class="btn btn-default" type="submit" disabled="disabled">Disconnect
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group">
                    <label for="name">What is your name?</label>
                    <input type="text" id="name" class="form-control" placeholder="Your name here...">
                </div>
                <button id="send" class="btn btn-default" type="submit">Send</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="conversation" class="table table-striped">
                <thead>
                <tr>
                    <th>Greetings</th>
                </tr>
                </thead>
                <tbody id="greetings">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
```



