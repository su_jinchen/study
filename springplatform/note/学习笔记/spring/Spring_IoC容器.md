# Spring IoC容器

## 概览

## 1.创建Spring项目

​	创建一个Maven管理的项目

![image-20200912114433868](Untitled.assets/image-20200912114433868.png)

![image-20200912114620167](Untitled.assets/image-20200912114620167.png)

​	pom文件中引入IoC容器的基础依赖

```xml
	<properties>
        <spring.version>5.2.8.RELEASE</spring.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.3.2</version>
            <scope>test</scope>
        </dependency>
        <!--spring核心模块-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <!--提供了配置框架和基本功能。-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <!--添加了更多企业特定的功能Spring IoC容器-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
    </dependencies>

```

> BeanFactory接口作为spring-beans中的一种高级配置机制，可以管理任何类型的对象。提供了配置框架和基本功能。
>
> ApplicationContext接口作为BeanFactory的子接口提供了更高级BeanFactory入口，添加了更多企业特定的功能，包括但不限于消息资源处理、时间发布以及特定于应用程序的上下文等。

## 2.容器了解

​	ApplicationContext代表Spring IoC容器，负责实例化，配置和组装bean的责任。因为是BeanFactory的子接口，所以它也可以管理通过容器注册的任何对象。Spring提供了ApplicationContext接口的几种实现方式，一般用的比较多的有以下三种：

- XML定义的容器

  ClassPathXmlApplicationContext

  FileSystemXmlApplicationContext

- 注解配置的容器

  AnnotationConfigApplicationContext


​	Spring容器通过读取配置元数据来获取有关要实例化、配置和组装哪些对象的指令，配置元数据的方式有以下三种：

1. XML
2. Java注解
3. Java代码

## 3.配置元数据

元数据解释

> ​	在 Spring 中，构成应用程序主干并由 Spring IoC *容器*Management 的对象称为* beans *。 Bean 是由 Spring IoC 容器实例化，组装和以其他方式 Management 的对象。否则，bean 仅仅是应用程序中许多对象之一。 Bean 及其之间的“依赖关系”反映在容器使用的“配置元数据”中。
>
> 配置元数据可以抽象的理解为Bean与Bean之间的关系，XML和Java代码的配置可读性相对较高，注解的方式过于分散。

配置实体

```java
package com.xiaopangzi.study.spring.metadata;

/**
 * 描述: 基于XML配置的bean
 *
 * @author : sujinchen
 * @date : 2020/9/12
 */
public class XmlMetadata {
    /**
     * id:标识号
     */
    private Long id;
    /**
     * name:名称
     */
    private String name;
    /**
     * describe：描述
     */
    private String describe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "XmlMetadata{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }
}

```

### 基于XML的元数据配置

spring.xml中装配bean

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd" >

    <bean id="xmlMetadata" class="com.xiaopangzi.study.spring.metadata.XmlMetadata">
        <property name="id" value="1"/>
        <property name="name" value="xmlMetadata"/>
        <property name="describe" value="基于XMl配置的元数据"/>
        <!-- collaborators and configuration for this bean go here -->
    </bean>
</beans>
```

使用容器

```java
        //从xml文件中加载资源到容器中
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        //从容器中获取Bean
        XmlMetadata bean =  context.getBean("xmlMetadata",XmlMetadata.class);
        System.out.println(bean);
```

> 容器的使用实际上就只有上面这关键的两步，当然了spring提倡的是我们不用主动的通过getBean的方式来获取容器中的对象，它提供了依赖注入的方式来供我们使用。

![image-20200913230938533](Spring_IoC容器.assets/image-20200913230938533.png)

> 上面的图为一个完整的bean拥有的属性，详细信息可到官网进行了解。
>
> https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html

### 基于Java代码的元数据配置

​	java代码装配bean，使用@Configuration和@Bean配合

```java
package com.xiaopangzi.study.spring.config;

import com.xiaopangzi.study.spring.metadata.XmlMetadata;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述: 基于java代码的配置
 *
 * @author : sujinchen
 * @date : 2020/9/13
 */
@Configuration
public class MetadataConfig {
    @Bean(name = {"xmlMetadata2"})
    public XmlMetadata getMetadata() {
        XmlMetadata xmlMetadata = new XmlMetadata();
        xmlMetadata.setId(2L);
        xmlMetadata.setName("getMetadata");
        xmlMetadata.setDescribe("基于java代码的配置");
        return xmlMetadata;
    }
}

```

使用容器

```java
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MetadataConfig.class);
        XmlMetadata xmlMetadata = context.getBean("xmlMetadata2", XmlMetadata.class);
        System.out.println(xmlMetadata);
```

### 基于java注解的元数据配置

实体上添加@Component注解，属性上配合使用@Value

```java
package com.xiaopangzi.study.spring.metadata;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 描述: 基于XML配置的bean
 *
 * @author : sujinchen
 * @date : 2020/9/12
 */
@Component("xmlMetadataJavaAnnotation")
public class XmlMetadata {
    /**
     * id:标识号
     */
    @Value("3L")
    private Long id;
    /**
     * name:名称
     */
    @Value("xmlMetadataJavaAnnotation")
    private String name;
    /**
     * describe：描述
     */
    @Value("基于java注解的配置")
    private String describe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "XmlMetadata{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }
}

```

配置类使用@ComponentScan扫描被@Component注解配置的类

```java
package com.xiaopangzi.study.spring.config;

import com.xiaopangzi.study.spring.metadata.XmlMetadata;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述: 基于java注解的配置
 *
 * @author : sujinchen
 * @date : 2020/9/13
 */
@Configuration
@ComponentScan(basePackageClasses = XmlMetadata.class)
public class MetadataConfig {
}

```

使用容器

```java
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MetadataConfig.class);
        XmlMetadata xmlMetadata = context.getBean("xmlMetadataJavaAnnotation", XmlMetadata.class);
        System.out.println(xmlMetadata);
```

## 4.Bean概述

### A：命名Bean

​	每个 bean 具有一个或多个标识符。这些标识符在承载 Bean 的容器内必须唯一。 Bean 通常只有一个标识符，但是如果需要多个标识符，则多余的标识符可以被视为别名。

#### 在基于 XML 的配置元数据中

​	使用`id`和/或`name`属性来指定 Bean 标识符。 `id`属性允许您精确指定一个 ID。按照惯例，这些名称是字母数字(“ myBean”，“ fooService”等)，但也可能包含特殊字符。如果要向 bean 引入其他别名，还可以在`name`属性中指定它们，并用逗号(`,`)，分号(`;`)或空格分隔。

> 如果未明确提供名称或 ID，则容器将为该 bean 生成一个唯一的名称。约定是在命名 bean 时将标准 Java 约定用于实例字段名称。也就是说，bean 名称以小写字母开头，并且此后以驼峰大小写。

##### 	引入别名的方式：

```xml
<alias name="fromName" alias="toName"/>
```

#### 在基于 Java代码的配置元数据中

​	默认情况下，bean 名称将与方法名称相同。以下是`@Bean`方法声明的简单示例：

```java
@Configuration
public class AppConfig {

    @Bean
    public TransferServiceImpl transferService() {
        return new TransferServiceImpl();
    }
}
```

​	等效于：

```xml
<beans>
    <bean id="transferService" class="com.acme.TransferServiceImpl"/>
</beans>
```

​	可以使用`name`属性覆盖此功能。

```java
@Configuration
public class AppConfig {

    @Bean(name = "myFoo")
    public Foo foo() {
        return new Foo();
    }
}
```

##### 引入别名的方式：

```java
@Configuration
public class AppConfig {

    @Bean({"dataSource", "subsystemA-dataSource", "subsystemB-dataSource"})
    public DataSource dataSource() {
        // instantiate, configure and return DataSource bean...
    }
}
```

#### 在基于 Java注解的配置元数据中

​	使用@Component标注的类默认使用类名作为bean的名称，可以通过设置value的值来替代。

```java
@Component("myComponentBean")
public class ComponentBean {
  
}
```

### B：实例化Bean

#### 使用构造函数实例化

XML中

```xml
<bean id="exampleBean" class="examples.ExampleBean"/>
```

java代码

```java
@Configuration
@ComponentScan(basePackageClasses = JavaBean.class)
public class JavaBeanConfig {
    @Bean(name = {"javaCodeBean"})
    public JavaBean javaBean() {
        //构造函数实例化
        JavaBean javaBean = new JavaBean();
        javaBean.setId(2L);
        javaBean.setName("基于java代码的配置Bean");
        return javaBean;
    }
}
```

@Component

默认采用默认的构造函数进行注入，不做解释。



#### 使用静态工厂方法实例化

XML

```java
public class StaticFactoryBean {
    private static StaticFactoryBean staticFactoryBean = new StaticFactoryBean();

    public static StaticFactoryBean create() {
        return staticFactoryBean;
    }
}
```

```xml
    <bean id="staticFactory"
          class="com.xiaopangzi.study.spring.bean.instance.StaticFactoryBean"
          factory-method="create"/>
```

java代码

```java
@Configuration
public class JavaStaticFactoryBean {

    private static JavaStaticFactoryBean javaStaticFactoryBean = new JavaStaticFactoryBean();
    @Bean
    public static JavaStaticFactoryBean getInstance() {
        return javaStaticFactoryBean;
    }
}
```

#### 使用实例工厂方法实例化

XML

```java
public class InstanceFactoryBean {
    private InstanceFactoryBean instanceFactoryBean = new InstanceFactoryBean();
    public InstanceFactoryBean createOne() {
        return this.instanceFactoryBean;
    }
    public InstanceFactoryBean createTwo() {
        return new InstanceFactoryBean();
    }
}
```

```xml
    <bean id="instanceFactoryBean" class="com.xiaopangzi.study.spring.bean.instance.InstanceFactoryBean"/>
    <bean id="createOne"
          factory-bean="instanceFactoryBean"
          factory-method="createOne"/>
    <bean id="createTwo"
          factory-bean="instanceFactoryBean"
          factory-method="createTwo"/>
```

java代码

```java
@Configuration
public class JavaInstanceFactoryBean {
    private static JavaStaticFactoryBean javaStaticFactoryBean = new JavaStaticFactoryBean();

    @Bean
    public JavaStaticFactoryBean getJavaStaticFactoryBean() {
        return javaStaticFactoryBean;
    }
}
```

## 5.依赖注入

依赖注入这里只分为两种方式，一种是采用xml进行注入配置，另外一种就是使用注解进行更细粒度的注入。

### 注入位置

#### A：构造函数注入

XML

```java
public class XmlBeanA {
    public XmlBeanA(BeanA beanA, BeanB beanB) {

    }
}

class BeanA {

}
class BeanB {

}
```

```xml
    <bean id="xmlBeanA" class="com.xiaopangzi.study.spring.bean.di.XmlBeanA">
        <constructor-arg ref="beanA"/>
        <constructor-arg ref="beanB"/>
    </bean>
    <bean id="beanA" class="com.xiaopangzi.study.spring.bean.di.BeanA"/>
    <bean id="beanB" class="com.xiaopangzi.study.spring.bean.di.BeanB"/>
```

注解@Autowired

```java
@Component
public class BeanC {
}

@Configuration
@ComponentScan(basePackageClasses = BeanC.class)
public class AnntactionBean {
    private BeanC beanC;

    @Autowired
    public AnntactionBean(BeanC beanC) {
    }
}
```

#### B：setter注入

xml

> *基于 Setter 的* DI 是通过在调用无参数构造函数或无参数`static`工厂方法以实例化您的 bean 之后，在 bean 上调用 setter 方法来完成的。可以参考使用静态工具方法实例化或者使用实例工厂方法实例化的方式！

注解@Autowired

```
    @Autowired
    public void setBeanC(BeanC beanC) {
        this.beanC = beanC;
    }
```

#### C：属性注入

xml

> 太简单不说了！

注解@Autowired

```java
    @Autowired
    private BeanC beanC;
```

> 关于XML的

### 注解注入的几种方式

#### 第一种：@Resource

​	Spring 还支持在字段或 bean 属性设置器方法上使用 JSR-250 `@Resource`注解进行注入。这是 Java EE 5 和 6 中的常见模式，例如在 JSF 1.2 托管 Bean 或 JAX-WS 2.0 端点中。 Spring 也为 SpringManagement 的对象支持此模式。

​	@Resource`具有名称属性，默认情况下，Spring 将该值解释为要注入的 Bean 名称。换句话说，它遵循* by-name *语义，如本示例所示：

```java
@Configuration
public class BeanResourceDI {
    @Resource
    private BeanResource beanResource;
    public BeanResourceDI() {
    }
}
```

> 如果未明确指定名称，则默认名称是从字段名称或 setter 方法派生的。如果是字段，则以字段名称为准；如果使用 setter 方法，则使用 bean 属性名称。

​	这种方式在标记时Spring不会提供是否注入成功的提示。

#### 第二种：@Autowired

​	使用@Autowired注解时有时需要搭配@Primary和@Qualifier注解进行使用，这里仅介绍@Primary和@Qualifier的一些用法

##### @Primary

> 使用@Primary 微调基于 Comments 的自动装配，由于按类型自动布线可能会导致多个候选对象，因此通常有必要对选择过程进行更多控制。实现此目的的一种方法是使用 Spring 的`@Primary`Comments。 `@Primary`表示当多个 bean 可以自动连接到单值依赖项的候选对象时，应优先考虑特定的 bean。如果候选对象中仅存在一个“主” bean，它将是自动装配的值。

​	简单的说就是存在多个候选的Bean的时候，通过在其中的候选Bean上使用@Primary标注就可以优先注入该Bean。

##### @Qualifier

当需要对选择过程进行更多控制时，可以使用 Spring 的`@Qualifier`Comments。您可以将限定符值与特定的参数相关联，从而缩小类型匹配的范围，以便为每个参数选择特定的 bean。最简单的情况下，这可以是简单的描述性值：

```java
public class MovieRecommender {

    @Autowired
    @Qualifier("main")
    private MovieCatalog movieCatalog;

    // ...
}
```

`@Qualifier`Comments 也可以在各个构造函数参数或方法参数上指定：

```java
public class MovieRecommender {

    private MovieCatalog movieCatalog;

    private CustomerPreferenceDao customerPreferenceDao;

    @Autowired
    public void prepare(@Qualifier("main")MovieCatalog movieCatalog,
            CustomerPreferenceDao customerPreferenceDao) {
        this.movieCatalog = movieCatalog;
        this.customerPreferenceDao = customerPreferenceDao;
    }

    // ...
}
```

可以创建自己的自定义限定符 Comments。只需定义一个 Comments 并在定义中提供`@Qualifier`Comments：

```java
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Genre {

    String value();
}
```

然后，可以在自动连接的字段和参数上提供自定义限定符：

```java
public class MovieRecommender {

    @Autowired
    @Genre("Action")
    private MovieCatalog actionCatalog;

    private MovieCatalog comedyCatalog;

    @Autowired
    public void setComedyCatalog(@Genre("Comedy") MovieCatalog comedyCatalog) {
        this.comedyCatalog = comedyCatalog;
    }

    // ...
}
```

> 还可以定义自定义限定符注解，除了简单的`value`属性之外，还可以接受命名属性。如果随后在要自动装配的字段或参数上指定了多个属性值，则 Bean 定义必须与所有“ *”这样的属性值匹配，才能被视为自动装配候选。

```java
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface MovieQualifier {

    String genre();

    Format format();
}


public enum Format {
    VHS, DVD, BLURAY
}


public class MovieRecommender {

    @Autowired
    @MovieQualifier(format=Format.VHS, genre="Action")
    private MovieCatalog actionVhsCatalog;

    @Autowired
    @MovieQualifier(format=Format.VHS, genre="Comedy")
    private MovieCatalog comedyVhsCatalog;

    @Autowired
    @MovieQualifier(format=Format.DVD, genre="Action")
    private MovieCatalog actionDvdCatalog;

    @Autowired
    @MovieQualifier(format=Format.BLURAY, genre="Comedy")
    private MovieCatalog comedyBluRayCatalog;

    // ...
}
```

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <context:annotation-config/>

    <bean class="example.SimpleMovieCatalog">
        <qualifier type="MovieQualifier">
            <attribute key="format" value="VHS"/>
            <attribute key="genre" value="Action"/>
        </qualifier>
        <!-- inject any dependencies required by this bean -->
    </bean>

    <bean class="example.SimpleMovieCatalog">
        <qualifier type="MovieQualifier">
            <attribute key="format" value="VHS"/>
            <attribute key="genre" value="Comedy"/>
        </qualifier>
        <!-- inject any dependencies required by this bean -->
    </bean>

    <bean class="example.SimpleMovieCatalog">
        <meta key="format" value="DVD"/>
        <meta key="genre" value="Action"/>
        <!-- inject any dependencies required by this bean -->
    </bean>

    <bean class="example.SimpleMovieCatalog">
        <meta key="format" value="BLURAY"/>
        <meta key="genre" value="Comedy"/>
        <!-- inject any dependencies required by this bean -->
    </bean>

</beans>
```

等同于

```java
@Component
@MovieQualifier(format=Format.BLURAY, genre="Comedy")
public class QualifierBean {
}
```

> 使用Java代码配置的方式，需要在注入的Bean上也标注相同的限定才行。比较Qualifier的作用就是加限定来区分注入的bean而已。

#### 第三种：@Inject

使用这种注入方式需要引入额外的依赖，也不做过多介绍，了解即可。

```xml
<dependency>
<groupId>javax.inject</groupId>
<artifactId>javax.inject</artifactId>
<version>1</version>
</dependency>
```

详情：https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-inject-named

![image-20200916164633218](Untitled.assets/image-20200916164633218.png)

### 注入时期-延迟初始化

> 默认情况下，作为初始化过程的一部分，`ApplicationContext`实现会基于创建和配置所有[singleton](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-factory-scopes-singleton) bean。通常，这种预初始化是可取的，因为与数小时甚至数天后相比，会立即发现配置或周围环境中的错误。如果此行为是“不希望的”，则可以通过将 bean 定义标记为延迟初始化来防止对 singleton bean 的预先实例化。延迟初始化的 bean 告诉 IoC 容器在首次请求时而不是在启动时创建一个 bean 实例。
>
> 当延迟初始化的 bean 是非延迟初始化的单例 bean 的依赖项时，`ApplicationContext`在启动时会创建延迟初始化的 bean，因为它必须满足单例的依赖关系。延迟初始化的 bean 被注入到其他未延迟初始化的单例 bean 中。

#### xml中

##### 	单个bean上延迟加载

```xml
    <bean id="beanc" class="com.xiaopangzi.study.spring.bean.di.BeanC" lazy-init="true"/>
```

##### 	多个bean上延迟加载

```xml
<beans default-lazy-init="true">
    <!-- no beans will be pre-instantiated... -->
</beans>
```

#### @Lazy

@LazyComments 还可以放置在标有@Autowired或@Inject的注入点上。在这种情况下，它导致注入了惰性解析代理。

```java
    @Bean
    @Lazy
    public JavaStaticFactoryBean getJavaStaticFactoryBean() {
        return javaStaticFactoryBean;
    }
    
    
@Component
@Lazy
public class BeanC {
}
```

## 6.Bean作用域

![image-20200916174244232](Untitled.assets/image-20200916174244232.png)

### 单例范围-singleton

​	定义一个 bean 定义并且其作用域为单例时，Spring IoC 容器会创建该 bean 定义定义的对象的“一个”实例。该单个实例存储在此类单例 bean 的缓存中，并且该命名 bean 的“所有后续请求和引用”返回缓存的对象。

![image-20200916174402651](Untitled.assets/image-20200916174402651.png)

xml中

```xml
    <bean id="beanc" class="com.xiaopangzi.study.spring.bean.di.BeanC" scope="singleton"/>
```

@Scope("singleton")

```java
@Scope("singleton")
public class BeanC {
}
```

> 默认情况就是单例的可以不用注释

### 原型范围-prototype

​	每次对特定 bean 发出请求时，bean 部署的非单一原型范围都会导致*创建一个新 bean 实例*。

![image-20200916175154774](Untitled.assets/image-20200916175154774.png)

xml中

```xml
    <bean id="bean2" class="com.xiaopangzi.study.spring.bean.di.BeanC" scope="prototype"/>
```

@Scope("prototype")

```java
@Scope("prototype")
public class BeanC {
}
```

> 当单例 Bean 需要与另一个单例 Bean 协作时，或者非单例 Bean 需要与另一个非单例 Bean 协作时，通常可以通过将一个 Bean 定义为另一个 Bean 的属性来处理依赖性。**但是，假设希望单例作用域的 bean 在运行时重复获取原型作用域的 bean 的新实例。您不能将原型作用域的 bean 依赖项注入到您的单例 bean 中，因为当 Spring 容器实例化单例 bean 并解析和注入其依赖项时，该注入仅* once *发生一次。**

单例作用域的 bean 在运行时重复获取原型作用域的 bean 的新实例

> 可以通过实现`ApplicationContextAware`接口来[使 bean A 知道容器](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-factory-aware)，并在每次 bean A 需要它时[对容器进行 getBean(“ B”)调用](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-factory-client)询问(通常是新的)bean B 实例。

```java
public class SingletonScopeBean implements ApplicationContextAware {
    private ApplicationContext context;
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    /**
     * 从容器中获取prototype类型的bean
     * @return
     */
    protected PrototypeScopeBean createPrototypeScopeBean() {
        return this.context.getBean("prototypeScopeBean", PrototypeScopeBean.class);
    }


    public void userPrototypeScopeBeanMethodOne() {
        //每次从中获取
        PrototypeScopeBean prototypeScopeBean = createPrototypeScopeBean();
    }
}
```

> 上面的原理实际上是放弃了控制反转，采用实现ApplicationContextAware的方式来让spring发现SingletonScopeBean并将其加入到容器中。生命周期中会详解。

### Request范围

xml中

```xml
<bean id="loginAction" class="com.foo.LoginAction" scope="request"/>
```

​	Spring 容器通过为每个 HTTP 请求使用`loginAction` bean 定义来创建`LoginAction` bean 的新实例。也就是说，`loginAction` bean 的作用域是 HTTP 请求级别。您可以根据需要更改创建实例的内部状态，因为从相同的`loginAction` bean 定义创建的其他实例将看不到状态的这些更改；因此，您可以更改实例的内部状态。它们特定于单个请求。当请求完成处理时，将限制作用于该请求的 Bean。

@RequestScope

```java
@RequestScope
@Component
public class LoginAction {
    // ...
}
```

### Session范围

xml中

```xml
<bean id="userPreferences" class="com.foo.UserPreferences" scope="session"/>
```

​	在单个 HTTP `Session`的生存期内，Spring 容器通过使用`userPreferences` bean 定义来创建`UserPreferences` bean 的新实例。换句话说，`userPreferences` bean 的作用域实际上是 HTTP `Session`级别。与`request-scoped` bean 一样，您可以根据需要任意更改所创建实例的内部状态，因为其他 HTTP `Session`实例(也使用从相同`userPreferences` bean 定义创建的实例)不会看到这些状态变化，因为它们特定于单个 HTTP `Session`。当最终丢弃 HTTP `Session`时，也将丢弃作用于该特定 HTTP `Session`的 bean。

@SessionScope

```java
@SessionScope
@Component
public class UserPreferences {
    // ...
}
```

### 全局会话范围

xml中

```xml
<bean id="userPreferences" class="com.foo.UserPreferences" scope="globalSession"/>
```

`		globalSession`范围类似于标准 HTTP `Session`范围([described above](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-factory-scopes-session))，并且仅在基于 Portlet 的 Web 应用程序的上下文中适用。 Portlet 规范定义了全局`Session`的概念，该概念在构成单个 Portlet Web 应用程序的所有 Portlet 之间共享。在`globalSession`范围内定义的 Bean 的作用域(或绑定)于全局 portlet `Session`的生存期。

### Application范围

xml中

```xml
<bean id="appPreferences" class="com.foo.AppPreferences" scope="application"/>
```

​	Spring 容器通过对整个 Web 应用程序使用`appPreferences` bean 定义来创建`AppPreferences` bean 的新实例。也就是说，`appPreferences` bean 的作用域为`ServletContext`级别，并存储为常规`ServletContext`属性。这有点类似于 Spring 单例 bean，但是有两个重要的区别：它是每个`ServletContext`而不是每个 Spring'ApplicationContext'(在任何给定的 Web 应用程序中可能都有多个)，并且实际上是公开的，因此可见为`ServletContext`属性。

@ApplicationScope

```java
@ApplicationScope
@Component
public class AppPreferences {
    // ...
}
```

### 自定义范围

bean 的作用域机制是可扩展的。您可以定义自己的范围，甚至重新定义现有范围，尽管后者被认为是不好的做法，并且您不能*覆盖内置的`singleton`和`prototype`范围。

> 要将自定义范围集成到 Spring 容器中，您需要实现`org.springframework.beans.factory.config.Scope`接口

以下方法从基础范围返回对象。例如，会话范围实现返回会话范围的 Bean(如果不存在，则该方法在将其绑定到会话以供将来参考之后，将返回该 Bean 的新实例)。

```java
Object get(String name, ObjectFactory objectFactory)
```

以下方法将对象从基础范围中删除。例如，会话范围实现从基本会话中删除了会话范围的 Bean。应该返回该对象，但是如果找不到具有指定名称的对象，则可以返回 null。

```java
Object remove(String name)
```

以下方法注册范围被销毁或范围中的指定对象销毁时范围应执行的回调。有关销毁回调的更多信息，请参考 javadocs 或 Spring 范围实现。

```java
void registerDestructionCallback(String name, Runnable destructionCallback)
```

以下方法获取基础范围的会话标识符。每个范围的标识符都不相同。对于会话范围的实现，此标识符可以是会话标识符。

```java
String getConversationId()
```

在编写并测试一个或多个自定义`Scope`实现之后，您需要使 Spring 容器意识到您的新作用域。以下方法是在 Spring 容器中注册新的`Scope`的中心方法：

```java
void registerScope(String scopeName, Scope scope);
```

此方法在`ConfigurableBeanFactory`接口上声明，该接口可通过 BeanFactory 属性随 Spring 附带的大多数具体`ApplicationContext`实现中使用。

`registerScope(..)`方法的第一个参数是与范围关联的唯一名称。在 Spring 容器本身中，此类名称的示例是`singleton`和`prototype`。 `registerScope(..)`方法的第二个参数是您希望注册和使用的自定义`Scope`实现的实际实例。



## 7.自定义Bean

​	要与容器对 bean 生命周期的 Management 进行交互，可以实现 Spring `InitializingBean`和`DisposableBean`接口。容器对前者调用`afterPropertiesSet()`，对后者调用`destroy()`，以允许 Bean 在初始化和销毁 Bean 时执行某些操作。

> 通常，在现代 Spring 应用程序中，JSR-250 `@PostConstruct`和`@PreDestroy`Comments 被认为是接收生命周期回调的最佳实践。使用这些 Comments 意味着您的 bean 没有耦合到特定于 Spring 的接口。有关详细信息，请参见[第 7.9.8 节“ @PostConstruct 和@PreDestroy”](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-postconstruct-and-predestroy-annotations)。
>
> 如果您不想使用 JSR-250 注解，但仍希望删除耦合，请考虑使用 init-method 和 destroy-method 对象定义元数据。

​	在内部，Spring 框架使用`BeanPostProcessor`实现来处理它可以找到的任何回调接口并调用适当的方法。如果您需要自定义功能或其他生命周期行为，Spring 并不提供现成的功能，则您可以自己实现`BeanPostProcessor`。

​	除了初始化和销毁回调，SpringManagement 的对象还可以实现`Lifecycle`接口，以便这些对象可以在容器自身生命周期的驱动下参与启动和关闭过程

### 生命周期回调

#### 初始化回调

​	org.springframework.beans.factory.InitializingBean`接口允许 Bean 在容器上设置了所有必需的属性后执行初始化工作。 `InitializingBean`接口指定一个方法：

```JAVA
void afterPropertiesSet() throws Exception;
```

​	建议不要使用`InitializingBean`接口，因为它不必要地将代码耦合到 Spring。或者，使用[@PostConstruct](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-postconstruct-and-predestroy-annotations)注解或指定 POJO 初始化方法。对于基于 XML 的配置元数据，可以使用`init-method`属性来指定具有无效无参数签名的方法的名称。通过 Java config，可以使用`@Bean`的`initMethod`属性，。例如，以下内容：

与spring代码耦合的两种方式：

```java
package com.xiaopangzi.study.spring.bean.customer;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 描述: 自定义Bean
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
@Component
public class CustomerBean implements InitializingBean {
    /**
     * 初始化回调,由InitializingBean接口定义
     * 缺点：将不必要代码耦合到Spring
     * @throws Exception
     */
    public void afterPropertiesSet() throws Exception {
        System.out.println("CustomerBean_>>>>>>afterPropertiesSet()");
    }

    /**
     * 初始化回调,由@PostConstruct注解标注
     * 缺点：将不必要代码耦合到Spring
     */
    @PostConstruct
    public void customerPostConstruct() {
        System.out.println("CustomerBean_>>>>>>customerPostConstruct()");

    }
}

```

不会与spring代码耦合的方式：

```java
@Configuration
@ComponentScan(basePackageClasses = CustomerBean.class)
public class CustomerBeanConfig {

    @Bean(initMethod = "initMethod")
    public CustomerBean customerBean() {
        return new CustomerBean();
    }
}


/**
 * 描述: 自定义Bean
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
public class CustomerBean implements InitializingBean {
    /**
     * 初始化回调，由init_method方式定义
     * 优点：不会将代码耦合到 Spring
     */
    public void initMethod() {
        System.out.println("CustomerBean_>>>>>>initMethod()");
    }
}

```

等同于

```xml
    <bean id="customerBean" class="com.xiaopangzi.study.spring.bean.customer.CustomerBean" init-method="initMethod"/>
```



#### 销毁回调

​	实现`org.springframework.beans.factory.DisposableBean`接口后，当包含 bean 的容器被销毁时，bean 可以获取回调。 `DisposableBean`接口指定一个方法：

```java
void destroy() throws Exception;
```

​	建议您不要使用`DisposableBean`回调接口，因为它不必要地将代码耦合到 Spring。或者，使用[@PreDestroy](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-postconstruct-and-predestroy-annotations)注解或指定 bean 定义支持的通用方法。对于基于 XML 的配置元数据，可以在`<bean/>`上使用`destroy-method`属性。

与spring代码耦合的两种方式：

```java
package com.xiaopangzi.study.spring.bean.customer;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 描述: 自定义Bean
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
public class CustomerBean implements InitializingBean, DisposableBean {
    
    /**
     * 销毁回调,由@PreDestroy注解标注
     * 缺点：将不必要代码耦合到Spring
     */
    @PreDestroy
    public void customerPreDestroy() {
        System.out.println("CustomerBean_>>>>>>customerPreDestroy()");

    }
    /**
     * 销毁回调,由DisposableBean接口定义
     * 缺点：将不必要代码耦合到Spring
     * @throws Exception
     */
    public void destroy() throws Exception {
        System.out.println("CustomerBean_>>>>>>destroy()");
    }
}

```

不会与spring代码耦合的方式：

```java
@Configuration
@ComponentScan(basePackageClasses = CustomerBean.class)
public class CustomerBeanConfig {

    @Bean(destroyMethod = "destoryMethod")
    public CustomerBean customerBean() {
        return new CustomerBean();
    }
}


/**
 * 描述: 自定义Bean
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
public class CustomerBean implements InitializingBean {
    /**
     * 销毁回调，由destroyMethod方式定义
     * 优点：不会将代码耦合到 Spring
     */
    public void destoryMethod() {
        System.out.println("CustomerBean_>>>>>>close()");
    }
}
```

等同于

```xml
<bean id="customerBean1" class="com.xiaopangzi.study.spring.bean.customer.CustomerBean" destroy-method="destoryMethod"/>
```

#### 默认的初始化和销毁方法

```xml
<beans default-init-method="init">

    <bean id="blogService" class="com.foo.DefaultBlogService">
        <property name="blogDao" ref="blogDao" />
    </bean>
    
    <!--消除默认初始化方法-->
    <bean id="blogService1" class="com.foo.DefaultBlogService" init-method="">
        <property name="blogDao" ref="blogDao" />
    </bean>
</beans>
```

> 如果需要批量，可以通过引入xml配置的方式来配置。

#### 组合生命周期机制

​	从 Spring 2.5 开始，有三个用于控制 bean 生命周期行为的选项：[InitializingBean](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-factory-lifecycle-initializingbean)和[DisposableBean](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-factory-lifecycle-disposablebean)回调接口；自定义`init()`和`destroy()`方法；和[@PostConstruct 和@PreDestroy 注解](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/beans.html#beans-postconstruct-and-predestroy-annotations)。您可以结合使用这些机制来控制给定的 bean。

​	以上这些配置的执行顺序如下：

> 为同一个 bean 配置的具有不同初始化方法的多种生命周期机制如下：
>
> - 用`@PostConstruct`Comments 的方法
> - `InitializingBean`回调接口定义的`afterPropertiesSet()`
> - 自定义配置的`init()`方法

> 销毁方法的调用 Sequences 相同：
>
> - 用`@PreDestroy`Comments 的方法
> - `DisposableBean`回调接口定义的`destroy()`
> - 自定义配置的`destroy()`方法

#### 启动和关闭回调

​	`Lifecycle`接口为具有生命周期要求的任何对象定义了基本方法(例如，启动和停止某些后台进程)：

```
public interface Lifecycle {

    void start();

    void stop();

    boolean isRunning();
}
```

任何 SpringManagement 的对象都可以实现该接口。然后，当`ApplicationContext`本身接收到开始和停止 signal 时，例如对于运行时的停止/重新启动方案，它将把这些调用级联到在该上下文中定义的所有`Lifecycle`实现。它通过委派`LifecycleProcessor`来实现：

```shell
public interface LifecycleProcessor extends Lifecycle {

    void onRefresh();

    void onClose();
}
```

请注意，`LifecycleProcessor`本身是`Lifecycle`接口的扩展。它还添加了两种其他方法来响应正在刷新和关闭的上下文。

> 请注意，常规`org.springframework.context.Lifecycle`接口只是用于显式启动/停止通知的简单约定，并不意味着在上下文刷新时自动启动。考虑实现`org.springframework.context.SmartLifecycle`，以便对特定 bean 的自动启动(包括启动阶段)进行细粒度的控制。另外，请注意，不能保证在销毁之前会发出停止通知：在常规关闭时，所有`Lifecycle` bean 都会在传播一般销毁回调之前首先收到停止通知；但是，在上下文生命周期中进行热刷新或中止刷新尝试时，仅将调用 destroy 方法。

## 8.感知接口Aware

### ApplicationContextAware

实现`org.springframework.context.ApplicationContextAware`接口的对象实例时，该实例将获得对该`ApplicationContext`的引用。

```java
public interface ApplicationContextAware extends Aware {

	void setApplicationContext(ApplicationContext applicationContext) throws BeansException;

}
```

> ​	因此，bean 可以通过`ApplicationContext`接口，或者通过将引用转换为该接口的已知子类(例如`ConfigurableApplicationContext`)，以编程方式操纵创建它们的`ApplicationContext`，从而公开了其他功能。一种用途是通过编程方式检索其他 bean。有时，此功能很有用；但是，通常应该避免使用它，因为它会将代码耦合到 Spring，并且不遵循“反转控制”样式，在这种样式中，将协作者作为属性提供给 bean。

​	从 Spring 2.5 开始，自动装配是获得对`ApplicationContext`的引用的另一种选择。 采用DI的方式，举个例子：


```java
@Component
public class GetApplicationContext {

    @Autowired
    public ApplicationContext getApplicationContext(ApplicationContext applicationContext) {
        return applicationContext;
    }
}
```

### BeanNameAware

实现`org.springframework.beans.factory.BeanNameAware`接口的类时，该类将获得对在其关联的对象定义中定义的名称的引用。

```java
public interface BeanNameAware {

    void setBeanName(String name) throws BeansException;
}
```

### 其他感知接口

| Name                             | Injected Dependency                                          |
| -------------------------------- | ------------------------------------------------------------ |
| `ApplicationContextAware`        | 声明`ApplicationContext`                                     |
| `ApplicationEventPublisherAware` | 附件`ApplicationContext`的事件发布者                         |
| `BeanClassLoaderAware`           | 类加载器，用于加载 Bean 类。                                 |
| `BeanFactoryAware`               | 声明`BeanFactory`                                            |
| `BeanNameAware`                  | 声明 bean 的名称                                             |
| `BootstrapContextAware`          | 资源适配器`BootstrapContext`容器在其中运行。通常仅在支持 JCA 的`ApplicationContext` s 中可用 |
| `LoadTimeWeaverAware`            | 定义* weaver *以便在加载时处理类定义                         |
| `MessageSourceAware`             | 解决消息的已配置策略(支持参数化和国际化)                     |
| `NotificationPublisherAware`     | Spring JMX 通知发布者                                        |
| `PortletConfigAware`             | 当前`PortletConfig`容器在其中运行。仅在可感知网络的 Spring `ApplicationContext`中有效 |
| `PortletContextAware`            | 当前`PortletContext`容器在其中运行。仅在可感知网络的 Spring `ApplicationContext`中有效 |
| `ResourceLoaderAware`            | 配置的加载器，用于对资源的低级访问                           |
| `ServletConfigAware`             | 当前`ServletConfig`容器在其中运行。仅在可感知网络的 Spring `ApplicationContext`中有效 |
| `ServletContextAware`            | 当前`ServletContext`容器在其中运行。仅在可感知网络的 Spring `ApplicationContext`中有效 |

## 9.容器扩展点

​	容器扩展点的作用不太理解，但是从spring的官方文档的文档树来看，到容器的扩展点SpringIoC以XML配置为核心的部分就已经结束了。回顾一下SpringIoC这部分的内容。

> 1. 以BeanFactory为开始，衍生了三种配置元数据的方式。
> 2. 了解Bean的命名方式以及实例化方式
> 3. 了解Bean的注入（位置、方式、时期）
> 4. 了解Bean的作用域范围
> 5. 了解如何自定义一个Bean以及Bean的生命周期
> 6. 了解Bean在程序中如何被感知的
> 7. 最后回到容器扩展点

​	万物开始即是结束，以此推测容器扩展点的作用在于给我们提供一种自定义的方式来配置BeanFactory，毕竟Spring官网一开始给了你默认的BeanFactory，后面学习的内容都是关于Bean的。容器扩展点中的内容也和BeanFactory有关。

### 使用 FactoryBean 自定义实例化逻辑

> 为本身是 factory *的对象实现`org.springframework.beans.factory.FactoryBean`接口。
>
> `FactoryBean`接口是可插入 Spring IoC 容器的实例化逻辑的点。如果拥有复杂的初始化代码，而不是(可能)冗长的 XML，可以用 Java 更好地表达，则可以创建自己的`FactoryBean`，在该类中编写复杂的初始化，然后将自定义`FactoryBean`插入容器。
>
> `FactoryBean`界面提供了三种方法：
>
> - `Object getObject()`：返回此工厂创建的对象的实例。实例可以共享，具体取决于该工厂是否返回单例或原型。
> - `boolean isSingleton()`：如果此`FactoryBean`返回单例，则返回`true`，否则返回`false`。
> - `Class getObjectType()`：返回`getObject()`方法或`null`返回的对象类型(如果事先未知)。
>
> `FactoryBean`概念和接口在 Spring 框架中的许多地方都使用过； Spring 本身提供了 50 多个`FactoryBean`接口的实现。
>
> 当您需要向容器请求一个实际的`FactoryBean`实例本身而不是它产生的 bean 时，请在调用`ApplicationContext`的`getBean()`方法时在该 bean 的 ID 前面加上“＆”符号(`&`)。因此，对于 ID 为`myBean`的给定`FactoryBean`，在容器上调用`getBean("myBean")`会返回`FactoryBean`的乘积；而调用`getBean("&myBean")`会返回`FactoryBean`实例本身。

### 使用 BeanFactoryPostProcessor 自定义配置元数据

> `	BeanFactoryPostProcessor`在* bean 配置元数据*上操作；也就是说，Spring IoC 容器允许`BeanFactoryPostProcessor`读取配置元数据，并可能在容器实例化`BeanFactoryPostProcessor`以外的任何 bean 之前*对其进行更改。

这个操作厉害，比如我们在这个时候调用第三方接口获得新的配置元数据来替换读取到的配置元数据，是不是就可以实现类似Spring Cloud读取远程配置中心文件的功能。

###  使用 BeanPostProcessor 自定义 bean

`BeanPostProcessor`接口定义了*回调方法*，可以实施这些回调方法以提供自己的(或覆盖容器的默认值)实例化逻辑，依赖关系解析逻辑等。如果想在 Spring 容器完成实例化，配置和初始化 bean 之后实现一些自定义逻辑，则可以插入一个或多个`BeanPostProcessor`实现。

> `BeanPostProcessor`对 bean(或对象)* instances *进行操作；也就是说，Spring IoC 容器实例化了一个 bean 实例，然后* then `BeanPostProcessor` s 进行工作。
>
> `BeanPostProcessor`范围是*每个容器*。仅在使用容器层次结构时才有意义。如果在一个容器中定义`BeanPostProcessor`，它将*仅*后处理该容器中的 bean。换句话说，一个容器中定义的 Bean 不会被另一个容器中定义的`BeanPostProcessor`后处理，即使这两个容器是同一层次结构的一部分。

#### 示例：Hello World，BeanPostProcessor 风格

```java
package scripting;

import org.springframework.beans.factory.config.BeanPostProcessor;

@Component
public class InstantiationTracingBeanPostProcessor implements BeanPostProcessor {

    // simply return the instantiated bean as-is
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean; // we could potentially return any object reference here...
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("Bean '" + beanName + "' created : " + bean.toString());
        return bean;
    }
}
```

#### 示例：RequiredAnnotationBeanPostProcessor

> 将回调接口或注解与自定义`BeanPostProcessor`实现结合使用是扩展 Spring IoC 容器的常用方法。一个示例是 Spring 的`RequiredAnnotationBeanPostProcessor`-`BeanPostProcessor`实现，该实现随 Spring 发行版一起提供，该实现可确保对(标有(任意)Comments 的)bean 上的 JavaBean 属性进行实际(配置)依赖注入值。