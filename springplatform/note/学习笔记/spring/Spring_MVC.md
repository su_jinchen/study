# Spring_MVC

> ​	Spring Web 模型视图控制器(MVC)框架是围绕DispatcherServlet设计的。该框架将请求分配给处理程序，并具有可配置的处理程序 Map，视图分辨率，语言环境，时区和主题分辨率，以及对文件上传的支持。默认处理程序基于`@Controller`和`@RequestMapping`注解，提供了多种灵活的处理方法。随着 Spring 3.0 的引入，`@Controller`机制还允许您通过`@PathVariable`注解和其他功能来创建 RESTful 网站和应用程序。