# Spring_AOP

> *面向方面的编程*(AOP)通过提供另一种思考程序结构的方式来补充面向对象的编程(OOP)。 OOP 中模块化的关键单位是类，而在 AOP 中模块化的单位是*方面*。方面使关注点模块化，例如跨多种类型和对象的事务 Management。 (在 AOP 文献中，此类关注点通常被称为“跨领域”关注点.)

​	OOP中每个类都是可以更改的，这样的好处是灵活，每个类都可以更改。但是在业务代码中，往往会存在一套不可以随意更改的流程，但这套流程中存在一块需要局部修改的内容。通过面向方面的编程可以抑制当业务代码需要部分改动时引起的连锁效应。缺点就是编程会相对比较死板和复杂。

## Spring AOP的术语

Ø **切面（aspect）**： 定义切点、通知以及引入的内容的地方，spring AOP通过切面的信息来增强Bean的功能或者将对应的方法织入流程。

> 用刀把一个西瓜分成两瓣，切开的切口就是切面；炒菜，锅与炉子共同来完成炒菜，锅与炉子就是切面。
>
> 需要替换或者增强的部分组件就是切面，登陆功能的加密用的MD5，需要更换加密方式，加密方式这里就是切面。

Ø **连接点（join point）**：对应具体被拦截的对象，spring只能支持方法，所以被拦截的对象指的是特定的方法。

Ø **切点（point cut）**：表示一组连接点，这些连接点通过统配、正则表达式集中起来，它定义了响应的Advice将要发生的地方。

Ø **通知（advice）**：定义了在切点（point cut）里定义的连接点（join point）需要做的具体操作，通过before、afte和around来区别是在每个joint point之前、之后还是代替执行。作为约定规则的具体流程实现。

Ø **目标对象（target）**：被代理的对象

Ø **引入（introduction）**：引入新的类和其方法，增强现有Bean的功能

Ø **织入（weaving）**：通过动态代理技术，为原有服务对象生成代理对象，然后将与切点定义匹配的连接器拦截，并按约定将各类通知织入约定流程的过程。

## AOP之约定编程

提供一定的约定规则，按照约定编程后，将自己开发的代码织入约定的流程中。根据以上的解释可以总结出以下几个步骤：

> a)    约定一个流程规则
>
> b)    生成代理对象与流程实现

### 约定流程规则：

自定义一个约定的流程规则，这个规则可以自己进行定义，如果是采用别人约定的流程，切记！听从他的规则，做个听话的乖宝宝，叛逆是没有好结果的。比如约定一个流程规则如下：

> a.    所有方法执行前都会执行before方法
>
> b.    如果useAround方法返回true，则执行拦截器的around方法，否则直接调用target对象的事件方法。
>
> c.    在完成事件后，都会执行after的方法。
>
> d.    在执行事件时如果发生异常就执行afterThrowing方法，否则就执行afterReturning方法。

![img](Spring_AOP.assets/clip_image002.jpg)

#### A.调用器类

```java
package com.xiaopangzi.study.spring.aop;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 描述: 调用器类，通过反射执行target对应方法
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class Invocation {
    /**
     * 参数列表
     */
    private Object[] params;
    /**
     * 反射执行方法
     */
    private Method method;
    /**
     * 传入的目标对象
     */
    private Object target;

    /**
     * 构造方法
     *
     * @param params 方法参数列表
     * @param method 单个方法
     * @param target 目标对象
     */
    public Invocation(Object[] params, Method method, Object target) {
        this.params = params;
        this.method = method;
        this.target = target;
    }

    /**
     * 通过反射调用目标对象的方法执行
     * @return object 目标对象调用结果
     * @throws InvocationTargetException 目标对象调用异常
     * @throws IllegalAccessException 非法存取异常
     */
    public Object proceed() throws InvocationTargetException, IllegalAccessException {
            return method.invoke(target, params);
    }


    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }
}

```

此类的重点在于proceed方法，这里实现了目标对象方法的调用。

#### B.规则接口类（拦截器）

```java
package com.xiaopangzi.study.spring.aop;

import java.lang.reflect.InvocationTargetException;

/**
 * 描述: 规则接口，作为自定义约定规则使用
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public interface Interceptor {
    /**
     * 所有事件执行前执行
     */
    void before();

    /**
     * 所有事件执行后执行
     */
    void after();


    /**
     * 是否执行around方法，为默认方法，默认不执行。
     * @return
     */
    default boolean useAround(){
        return false;
    }

    /**
     * 调用目标对象的方法,默认调用
     * @param invocation
     * @return 目标对象方法调用结果
     * @throws InvocationTargetException 目标对象调用异常
     * @throws IllegalAccessException 非法存取异常
     */
    default Object around(Invocation invocation) throws InvocationTargetException, IllegalAccessException{
        return invocation.proceed();
    }

    /**
     * 事件执行后，无异常执行此方法。
     */
    void afterReturn();

    /**
     * 事件执行后，出现异常执行此方法。
     */
    void afterThrowing();

}

```

```java
package com.xiaopangzi.study.spring.aop.impl;

import com.xiaopangzi.study.spring.aop.Interceptor;
import com.xiaopangzi.study.spring.aop.Invocation;

import java.lang.reflect.InvocationTargetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述: AOP拦截器自定义实现，
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class AopInterceptorImpl implements Interceptor {

    @Override
    public void before() {
        System.out.println("before事件执行>>{}"+this.getClass().getName());
    }

    @Override
    public void after() {
        System.out.println("after>>{}"+this.getClass().getName());

    }

    @Override
    public boolean useAround() {
        System.out.println("after>>{}"+this.getClass().getName());
        return false;
    }

    @Override
    public Object around(Invocation invocation) throws InvocationTargetException, IllegalAccessException {
        System.out.println("around.before>>{}"+this.getClass().getName());
        Object proceed = null;
        invocation.proceed();
        System.out.println("around.after>>{}"+this.getClass().getName());
        return proceed;
    }

    @Override
    public void afterReturn() {
        System.out.println("afterReturn>>{}"+this.getClass().getName());

    }

    @Override
    public void afterThrowing() {
            System.out.println("afterThrowing>>{}"+this.getClass().getName());
    }
}

```

#### C.约定流程以及代理对象获取

```java
package com.xiaopangzi.study.spring.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 描述: AOP约定流程规范，绑定目标实体与拦截器
 *  InvocationHandler>>该接口只有一个方法，通过目标对象(proxy)、方法和参数就可以反射运行方法获取结果。
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class ProxyBean implements InvocationHandler {
    /**
     * 拦截器
     */
    Interceptor interceptor;
    /**
     * 目标对象，被代理的对象
     */
    Object target;

    /**
     * 构造函数，绑定目标对象和拦截器
     * @param interceptor 拦截器
     * @param target 目标对象
     */
    public ProxyBean(Interceptor interceptor, Object target) {
        this.interceptor = interceptor;
        this.target = target;
    }

    /**
     * 获取代理对象，由JDK方式获取代理对象
     * @return 代理对象
     */
    public Object getProxyBean() {
        return Proxy.newProxyInstance(target.getClass().getClassLoader()
                , target.getClass().getInterfaces(), this);
    }
    
    /**
     * 约定流程规则
     *
     * @param proxy  代理对象
     * @param method 对象方法
     * @param args   参数列表
     * @return 目标对象方法调用结果
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //通过目标对象生成调用器
        Invocation invocation = new Invocation(args, method, target);
        //保存调用目标对象方法返回结果
        Object result = null;
        boolean execeptionFlag = false;
        /**
         * 约定流程实现
         */
        this.interceptor.before();
        try {
            if (this.interceptor.useAround()) {
                this.interceptor.around(invocation);
            } else {
                result = invocation.proceed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            execeptionFlag = true;
        }
        if (execeptionFlag) {
            this.interceptor.afterThrowing();
        } else {
            this.interceptor.afterReturn();
        }
        this.interceptor.after();
        return result;
    }
}

```

#### D.目标对象代码

```java
package com.xiaopangzi.study.spring.aop;

/**
 * 描述: 目标对象接口
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public interface HellowService {
    void say();
}


package com.xiaopangzi.study.spring.aop.impl;

import com.xiaopangzi.study.spring.aop.HellowService;

/**
 * 描述: 目标对象接口
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class HellowServiceImpl implements HellowService {
    @Override
    public void say() {
        System.out.println(" I am target!");
    }
}

```

#### E.代码测试

```java
package com.xiaopangzi.study.spring.aop;

import com.xiaopangzi.study.spring.aop.impl.AopInterceptorImpl;
import com.xiaopangzi.study.spring.aop.impl.HellowServiceImpl;


/**
 * 描述: 测试类
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
class ProxyBeanTest {
    public static void main(String[] args) {
        //生成目标对象
        HellowService hellowService = new HellowServiceImpl();
        //绑定目标对象和拦截器
        ProxyBean proxyBean = new ProxyBean(new AopInterceptorImpl(), hellowService);
        //生成代理对象
        HellowService proxy = (HellowService) proxyBean.getProxyBean();
        //代理对象调用方法
        proxy.say();
    }

}
```

### 小结：

1. Invocation（调用器类）负责通过反射调用目标对象的具体方法并返回结果
2. Intercepter（拦截器）实现拦截器可定义具体操作步骤的明细
3. InvocationHandler（调用处理类）实现该接口，并在invoke方法中约定具体的流程规则。
4. ProxyBean实现InvocationHandler类并将目标对象和拦截器进行绑定，并且提供一个返回代理对象的方法供开发人员使用。

## @AspectJ支持

> @AspectJ 是一种将方面声明为带有 Comments 的常规 Java 类的样式。 @AspectJ 样式是[AspectJ project](https://www.eclipse.org/aspectj)作为 AspectJ 5 版本的一部分引入的。 Spring 使用 AspectJ 提供的用于切入点解析和匹配的库来解释与 AspectJ 5 相同的 Comments。但是，AOP 运行时仍然是纯 Spring AOP，并且不依赖于 AspectJ 编译器或编织器。

### 快速开始

1. ### 启用@AspectJ支持

   > ```java
   > package com.xiaopangzi.study.spring.aspectj.config;
   > 
   > import com.xiaopangzi.study.spring.aspectj.bean.MyAspectBean;
   > import org.springframework.context.annotation.Bean;
   > import org.springframework.context.annotation.ComponentScan;
   > import org.springframework.context.annotation.Configuration;
   > import org.springframework.context.annotation.EnableAspectJAutoProxy;
   > 
   > /**
   >  * 描述: 通过 Java 配置启用@AspectJ 支持
   >  *
   >  * @author : sujinchen
   >  * @date : 2020/9/27
   >  */
   > @Configuration
   > @EnableAspectJAutoProxy
   > @ComponentScan("com.xiaopangzi.study.spring.aspectj")
   > public class AspectJConfig {
   > 
   >     /**
   >      * 注入切面
   >      * @return
   >      */
   >     @Bean
   >     public MyAspectBean myAspectBean() {
   >         return new MyAspectBean();
   >     }
   > }
   > 
   > ```

2. 声明切面

   > 启用@AspectJ 支持后，Spring 会自动检测在应用程序上下文中使用@AspectJ 方面(具有`@Aspect`Comments)的类定义的任何 bean，并将其用于配置 Spring AOP。

   ```java
   package com.xiaopangzi.study.spring.aspectj.bean;
   import org.aspectj.lang.annotation.Aspect;
   import org.aspectj.lang.annotation.Pointcut;
   
   /**
    * 描述: 声明一个切面，切面支持定义切点、建议等
    *
    * @author : sujinchen
    * @date : 2020/9/27
    */
   @Aspect
   public class MyAspectBean {
   }
   
   ```

3. 声明切入点

   ```java
       @Pointcut("execution(* com.xiaopangzi.study.spring..*.say())")
       public void sayPonitCut() {
           System.out.println("sayPonitCut>>>开始执行切入点");
       }
   ```

   组合切入点表达式：

   ```java
   @Pointcut("execution(public * *(..))")
   private void anyPublicOperation() {}
   
   @Pointcut("within(com.xyz.someapp.trading..*)")
   private void inTrading() {}
   
   @Pointcut("anyPublicOperation() && inTrading()")
   private void tradingOperation() {}
   ```

   

4. 声明建议

   ```java
   @Before("execution(* com.xiaopangzi.study.spring.aspectj.bean.SayServiceImpl.say())")
       public void before() {
           System.out.println("MyAspectBean>>>before");
       }
   
       @After("sayPonitCut()")
       public void after() {
           System.out.println("MyAspectBean>>>after");
       }
   
       @AfterReturning("sayPonitCut()")
       public void afterReturn() {
           System.out.println("MyAspectBean>>>afterReturn");
   
       }
   
       @AfterThrowing("sayPonitCut()")
       public void afterThrowing() {
           System.out.println("MyAspectBean>>>afterThrowing");
   
       }
   
       @Around("sayPonitCut()")
       public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
           System.out.println("MyAspectBean>>>around.before");
           Object proceed = joinPoint.proceed();
           System.out.println("MyAspectBean>>>around.after");
   
           return proceed;
       }
   ```

   

5. 测试

   ```java
   package com.xiaopangzi.study.spring.aspectj.bean;
   
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.stereotype.Service;
   
   /**
    * 描述:
    *
    * @author : sujinchen
    * @date : 2020/9/27
    */
   @Service
   public class TestBean {
       @Autowired
       SayService sayService;
   
       public void doSay() {
           sayService.say();
       }
   
   }
   
   ```

   ```java
   package com.xiaopangzi.study.spring.aspectj.bean;
   
   import com.xiaopangzi.study.spring.aspectj.config.AspectJConfig;
   import org.springframework.context.annotation.AnnotationConfigApplicationContext;
   
   import static org.junit.jupiter.api.Assertions.*;
   
   /**
    * 描述:
    *
    * @author : sujinchen
    * @date : 2020/9/27
    */
   class SayServiceImplTest {
       public static void main(String[] args) {
           AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AspectJConfig.class);
           TestBean sayService = context.getBean("testBean", TestBean.class);
           sayService.doSay();
       }
   }
   ```

   

### A.方面

1. 使用@Aspect声明一个方面
2. 使用@Configuration以及@EnableAspectJAutoProxy配置启用@AspectJ支持
3. 将被@Aspect声明的方面注入Spring容器中，注入方式不限，建议在切面配置中注入

### B.切入点

> *切入点声明由两部分组成：一个包含名称和任何参数的签名，以及一个切入表达式，该表达式精确地确定我们感兴趣的方法执行。在 AOP 的@AspectJ 注解样式中，切入点签名由 AOP 提供。常规方法定义，并使用`@Pointcut`Comments 指示切入点表达式(用作切入点签名的方法*必须*具有`void`返回类型)。
>
> ```java
> @Pointcut("execution(* transfer(..))")// 切点表达式
> private void anyOldTransfer() {}// 切点签名
> ```
>
> 切点表达式：指向需要被拦截的具体方法
>
> 切点签名：定义一个切点名称供建议使用

1. 使用@Pointcut声明一个切点，切点签名由方法名称定义。

2. 切点表达式比较重要，内容也是比较多的一部分，可参考以下连接学习：

   > ```
   > https://www.eclipse.org/aspectj/doc/released/progguide/index.html
   > https://www.eclipse.org/aspectj/doc/released/adk15notebook/index.html
   > ```

C.建议

1. @Before建议

2. @After建议

3. @AfterReturn建议

4. @AfterThrowing建议

5. @Around建议

6. 建议参数

   1. JoinPoint->>ProceedingJoinPoint作为连接点可以调用目标对象方法

      1. 任何通知方法都可以将`org.aspectj.lang.JoinPoint`的类型声明为其第一个参数
      2. @Around建议需将ProceedingJoinPoint的类型声明为其第一个参数

   2. 切点表达式中使用args可将aop代理的方法参数传递给建议使用

      ```java
          //args(a,..)表示它将匹配限制为仅方法采用至少一个参数，并且传递给该参数的参数是Account的实例的方法执行；其次，它通过account参数使实际的Account对象可用于建议。
          @Pointcut("execution(* com.xiaopangzi.study.spring..*.say(..))&&args(a,..)")
          public void sayPonitCut(String a) {
              System.out.println("sayPonitCut>>>开始执行切入点");
          }
          
          @After("sayPonitCut(a)")
          public void after(String a) {
              System.out.println("MyAspectBean>>>after->"+a);
          }
          
      ```

      

7. 建议顺序

   1. 同一切面，当多条建议在同一个连接点上运行时，优先级高的最先运行。

      1. 考虑将这些建议方法折叠为每个方面类中每个连接点的一个建议方法，或将建议重构为单独的方面类-可以在方面级别进行排序。

   2. 不同切面中定义的建议在同一连接点上运行时，除非另外指定，否则切面执行顺序是不确定的。

      1. 通过在切面类中实现`org.springframework.core.Ordered`接口或使用`Order`注解对来指定优先级。`Ordered.getValue()`返回较低值的方面具有较高的优先级。

      

## 代理机制

Spring AOP 使用 JDK 动态代理或 CGLIB 创建给定目标对象的代理。 (只要有选择，首选 JDK 动态代理)。

如果要代理的目标对象实现至少一个接口，则将使用 JDK 动态代理。目标类型实现的所有接口都将被代理。如果目标对象未实现任何接口，则将创建 CGLIB 代理。

如果要强制使用 CGLIB 代理(例如，代理为目标对象定义的每个方法，而不仅仅是代理由其接口实现的方法)，则可以这样做。但是，有一些问题要考虑：

> - 不能建议`final`方法，因为它们不能被覆盖。
> - 从 Spring 3.2 开始，不再需要将 CGLIB 添加到您的项目 Classpath 中，因为 CGLIB 类在 org.springframework 下重新打包并直接包含在 spring-core JAR 中。这意味着基于 CGLIB 的代理支持“可以正常工作”，就像 JDK 动态代理始终具有的一样。
> - 从 Spring 4.0 开始，由于将通过 Objenesis 创建 CGLIB 代理实例，因此将不再两次调用您的代理对象的构造函数。仅当您的 JVM 不允许绕过构造函数时，您才可能从 Spring 的 AOP 支持中看到两次调用和相应的调试日志条目

```java
//强制CGLIB代理
@EnableAspectJAutoProxy(proxyTargetClass = true)
```

## AOP自调用

AOP自调用存在一个问题就是，方法A、方法B都被AOP代理，采用代理对象调用方法A和方法B时，AOP拦截起作用。但是在方法A中调用方法B时，方法B的执行不受AOP代理管理。例子如下：

非自调用情况：

```java
@Component
public class SayServiceImpl implements SayService {
    @Override
    public void say(String a,String b) {
        System.out.println("你好！");
    }

    @Override
    public void sayB(String a, String b) {
        System.out.println("你好B！");
    }
}
@Service
public class TestBean {
    @Autowired
    SayService sayService;

    public void doSay() {
        sayService.say("哼！","嗨");
        sayService.sayB("哼！B","嗨B");
    }

}
运行结果：
你好！
MyAspectBean>>>after->哼！
你好B！
MyAspectBean>>>after->哼！B
```

自调用情况：

```java
@Component
public class SayServiceImpl implements SayService {
    @Override
    public void say(String a,String b) {
        System.out.println("你好！");
        sayB("B>>"+a,"B>>"+b);
    }

    @Override
    public void sayB(String a, String b) {
        System.out.println("你好B！");
    }
}

@Service
public class TestBean {
    @Autowired
    SayService sayService;

    public void doSay() {
        sayService.say("哼！","嗨");
    }

}
运行结果：
你好！
你好B！
MyAspectBean>>>after->哼！
```

原因：

> AOP织入建议的方式是对调用的目标方法之前、之后、环绕等方式进行织入的。具体的增强操作由AOP代理提供，并非是目标对象的方法。当代理对象调用目标对象的方法时才会涉及到目标对象的实际调用。试想一下，真正的目标对象调用自己的方法，作为经纪人的AOP还有必要去插手嘛？

举个例子：

```java
package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class TargetBean {
    /**
     * 目标对象的败家方法
     */
    public Object spendMoney () throws Throwable {
        System.out.println("千万富翁路，败家才能行！");
        return true;
    }
}

```

```java
package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象的经纪人
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class ProxyBean extends TargetBean {
    /**
     * 经纪人的科学败家
     */
    @Override
    public Object spendMoney() {
        //前置增强
        System.out.println("before:我是经纪人!");
        //保存目标对象方法执行结果
        Object result = null;
        //异常标志位
        boolean exceptionFlag = false;
        try {
            //目标对象方法调用
            result = super.spendMoney();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            exceptionFlag = true;
        }
        if (exceptionFlag) {
            //抛出问题增强
            System.out.println("afterThrowing:老板破产了！");
        }
        //返回增强
        if (result == null || result.equals("")) {
            System.out.println("afterReturn:老板吹牛呢，不打钱败什么家！");
        }
        //后置增强
        System.out.println("after:我来帮助老板科学败家！");
        return result;
    }

    public static void main(String[] args) {
        ProxyBean proxyBean = new ProxyBean();
        proxyBean.spendMoney();
    }
}
运行结果：
	before:我是经纪人!
	target:千万富翁路，败家才能行！
	after:我来帮助老板科学败家！
```

以上就是一个最最简单的代理方式，如果在目标对象中再添加一个savemoney方法，并且在每次败家前都先savemoney。看看会出现什么样的情况。

```java
package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class TargetBean {
    /**
     * 目标对象的败家方法
     */
    public Object spendMoney () throws Throwable {
        saveMoney();
        System.out.println("target:千万富翁路，败家才能行！");
        return true;
    }

    /**
     * 目标对象的赚钱方法
     * @return
     */
    public Object saveMoney() {
        System.out.println("target:继承家产！");
        return true;
    }
    
        public static void main(String[] args) {
        ProxyBean proxyBean = new ProxyBean();
        proxyBean.spendMoney();
    }
}
运行结果：
	before:我是经纪人!
	target:继承家产！
	target:千万富翁路，败家才能行！
	after:我来帮助老板科学败家！
```

​	显而易见，目标对象调用自己的方法时不受代理对象的管理，毕竟代理对象管的只是如何帮目标对象败家，它可管不了如何帮目标对象赚钱！所以这样的设计在实际上时合理的。如果想要管目标对象赚钱的方式，最好的办法就是经纪人扩展自己的业务范围（新增一个savemoney方法），并将目标对象需要做的动作都安排好。这样做有以下几点好处：

1. 目标对象只需要关心两件事，败家和要钱！不用关心自己有多少钱。

2. 代理对象负责关心两件事，钱太多时如何败家以及钱不够时如何科学要钱。

3. 最重要得一点来了，之前得目标对象需要记住自己每次败家前先去要点钱。有了经纪人就不用管啦，经纪人在小本本上写的好好得。败家前先去要钱。以后目标对象再有其他得业务出来，经纪人只需要去小本本上改改顺序即可。

   > 其实这里就很好得将业务逻辑和代码实现给抽离出来了。具体的业务流程单独提出来，经纪人、目标对象只需要关心自己的具体业务即可。

总结：记好了AOP的核心是代理模式，让经纪人主动告诉老板该做什么，具体的业务流程小本本记好。这样的经纪人才是一个合格受老板喜欢的经纪人！如果老板要做的事情还要主动和经纪人讲，那么这个经纪人实在是说不上有多称职。

```java
package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class TargetBean {
    /**
     * 目标对象的败家方法
     */
    public Object spendMoney () throws Throwable {
        saveMoney();
        System.out.println("target:千万富翁路，败家才能行！");
        return true;
    }

    /**
     * 目标对象的赚钱方法
     * @return
     */
    public Object saveMoney() {
        System.out.println("target:继承家产！");
        return true;
    }

    /**
     * 目标对象主动告诉经纪人该做啥
     * @return
     */
    public Object tellProxyDo(ProxyBean proxyBean) {
        System.out.println("target:经纪人你该做啥！");
        proxyBean.spendMoney();
        return true;
    }

    public Object getInfo(ProxyBean proxyBean) {
        return tellProxyDo(proxyBean);
    }
}
package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象的经纪人
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class ProxyBean extends TargetBean {
    /**
     * 经纪人的科学败家
     */
    @Override
    public Object spendMoney() {
        //前置增强
        System.out.println("before:我是经纪人!");
        //保存目标对象方法执行结果
        Object result = null;
        //异常标志位
        boolean exceptionFlag = false;
        try {
            //目标对象方法调用
            result = super.spendMoney();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            exceptionFlag = true;
        }
        if (exceptionFlag) {
            //抛出问题增强
            System.out.println("afterThrowing:老板破产了！");
        }
        //返回增强
        if (result == null || result.equals("")) {
            System.out.println("afterReturn:老板吹牛呢，不打钱败什么家！");
        }
        //后置增强
        System.out.println("after:我来帮助老板科学败家！");
        return result;
    }



    @Override
    public Object getInfo(ProxyBean proxyBean) {
        System.out.println("before：获取信息！");
        return super.getInfo(proxyBean);
    }
    public static void main(String[] args) {
        ProxyBean proxyBean = new ProxyBean();
        proxyBean.getInfo(proxyBean);
    }
}
运行结果	
	before：获取信息！
	target:经纪人你该做啥！
	before:我是经纪人!
	target:继承家产！
	target:千万富翁路，败家才能行！
	after:我来帮助老板科学败家！
```

看吧，是不是很糟糕，目标对象中穿插了代理对象的代码，业务逻辑参杂在一起，实在不是一个好的体验。