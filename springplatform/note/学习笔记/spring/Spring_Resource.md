# Spring_Resource

## 介绍

> Java 的标准`java.net.URL`类和用于各种 URL 前缀的标准处理程序不足以满足所有对低级资源的访问。例如，没有标准化的`URL`实现可用于访问需要从 Classpath 或相对于`ServletContext`获得的资源。尽管可以为专用的`URL`前缀注册新的处理程序(类似于现有的针对`http:`的前缀的处理程序)，但这通常相当复杂，并且`URL`接口仍然缺少某些理想的功能，例如用于检查是否存在`URL`的方法。指向的资源。

​	简单的讲就是java原生的做的不那么人性话，Spring来做一个增强，提供一种功能更强大的接口，用于抽象化对低级资源的访问。

> 注意：Resource仅仅提供了对低级资源的访问，并将访问到的资源以特殊的方式集成到Spring中而已，这些处理可以用自己写好的资源访问工具来代替，所以Spring_Resource的学习可以了解为主，有时间还是多去看看Java原生的IO操作。

```java
package org.springframework.core.io;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import org.springframework.lang.Nullable;

public interface Resource extends InputStreamSource {
    boolean exists();

    default boolean isReadable() {
        return this.exists();
    }

    default boolean isOpen() {
        return false;
    }

    default boolean isFile() {
        return false;
    }

    URL getURL() throws IOException;

    URI getURI() throws IOException;

    File getFile() throws IOException;

    default ReadableByteChannel readableChannel() throws IOException {
        return Channels.newChannel(this.getInputStream());
    }

    long contentLength() throws IOException;

    long lastModified() throws IOException;

    Resource createRelative(String var1) throws IOException;

    @Nullable
    String getFilename();

    String getDescription();
}

```

`Resource`界面中一些最重要的方法是：

- `getInputStream()`：找到并打开资源，返回`InputStream`以从资源中读取。预计每次调用都返回一个新的`InputStream`。呼叫者有责任关闭流。
- `exists()`：返回`boolean`，指示此资源是否实际以物理形式存在。
- `isOpen()`：返回一个`boolean`，指示此资源是否代表具有打开流的句柄。如果为`true`，则不能多次读取`InputStream`，并且只能读取一次，然后关闭该`InputStream`以避免资源泄漏。对于所有常规资源实现，将为`false`，但`InputStreamResource`除外。
- `getDescription()`：返回此资源的描述，用于在处理资源时输出错误。这通常是标准文件名或资源的实际 URL。

> 虽然`Resource`接口在 Spring 和 Spring 上经常使用，**但是在自己的代码中单独用作通用工具类来访问资源实际上非常有用**，即使你的代码不知道或不关心其他任何代码Spring的一部分。虽然这将你的代码耦合到 Spring，但实际上仅将其耦合到这套 Util 小类，这些 Util 类是`URL`的更强大的替代品，可以被认为等同于你将用于此目的的任何其他库。

## 内置资源实现

UrlResource

`UrlResource`包装`java.net.URL`，可用于访问通常可通过 URL 访问的任何对象，例如文件，HTTP 目标，FTP 目标等。所有 URL 均具有标准化的`String`表示形式，因此可以使用适当的标准化前缀用来表示另一种 URL 类型。这包括`file:`用于访问文件系统路径，`http:`用于通过 HTTP 协议访问资源，`ftp:`用于通过 FTP 访问资源等。

`UrlResource`是由 Java 代码使用`UrlResource`构造函数显式创建的，但通常在调用带有`String`参数(表示路径)的 API 方法时隐式创建。对于后一种情况，JavaBeans `PropertyEditor`将最终决定要创建的`Resource`类型。如果路径字符串包含一些众所周知的前缀(例如`classpath:`)，它将为该前缀创建一个适当的专用`Resource`。但是，如果无法识别前缀，它将假定这只是一个标准 URL 字符串，并会创建`UrlResource`。

```java
package com.xiaopangzi.study.spring.resource;

import org.springframework.core.io.UrlResource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/19
 */
public class UrlResourceDemo {
    public static void main(String[] args) {
        try {
            UrlResource urlResource = new UrlResource("https://www.cnblogs.com/ddwarehouse/p/10127729.html");
            InputStream inputStream = urlResource.getInputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            while((len = inputStream.read(buffer)) != -1) {
                bos.write(buffer, 0, len);
            }
            bos.close();
            byte[] result = bos.toByteArray();
            String str = new String(result);
            System.out.println ("打印内容："+str);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

```

> 从这里可以看出，仅仅提供给我们一个更优雅的访问资源的方式，读流等操作还是需要自己实现。