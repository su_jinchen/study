# Spring_WebSocket_Client

​	关于websocket服务端有Spring和javax包的实现。至于客户端的话，目前流行的是java-websocket和okhttp框架。

## Java-websocket

```java
package com.cvte.rt.wsclient;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.enums.ReadyState;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * 描述: WebSocket客户端，代替HTML中的请求实现
 * 由java-websocket组件提供
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
public class MyWebSocketClient extends WebSocketClient {
    private Logger logger = LoggerFactory.getLogger(MyWebSocketClient.class);
    private static Long total = 0L;
    private static Long succeed = 0L;
    private static Long faile = 0L;

    public static Long getTotal() {
        return total;
    }

    public static Long getSucceed() {
        return succeed;
    }

    public static Long getFaile() {
        return faile;
    }

    public MyWebSocketClient(URI serverUri) {
        super(serverUri);
    }

    public MyWebSocketClient(URI serverUri, Draft protocolDraft) {
        super(serverUri, protocolDraft);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        short httpStatus = serverHandshake.getHttpStatus();
        String httpStatusMessage = serverHandshake.getHttpStatusMessage();
        logger.info("握手成功！握手状态：{} 握手状态信息：{}", httpStatus, httpStatusMessage);
    }

    @Override
    public void onMessage(String s) {
        logger.info("收到的消息：{}",s);
        String msg = "用户:发送命令:" + total;
        System.out.println("【msg】"+msg);
        if (s.equals(msg)) {
            succeed++;
        } else {
            faile++;
        }
        total++;
        if (total == 9) {
            System.out.println(toString());
            this.close();
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        logger.info("链接已关闭！i:{},s:{},b:{}",i,s,b);
    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
        logger.info("连接出现异常！异常信息：{}",e.getMessage());
    }

    @Override
    public String toString() {
        return "MyWebSocketClient{总请求数："+total
                +" 成功数量："+succeed
                +" 失败数量："+faile
                +"}";
    }

    public static void main(String[] args) {
        Draft_6455 draft_6455 = new Draft_6455();
        try {
            MyWebSocketClient myWebSocketClient = new MyWebSocketClient(new URI("ws://localhost:8080/myHandler"), draft_6455);
            myWebSocketClient.connect();
            while (!myWebSocketClient.getReadyState().equals(ReadyState.OPEN)) {
                System.out.println("正在连接！");
            }
            System.out.println("建立websocket连接");

            for (int i = 0; i < 10; i++) {
                myWebSocketClient.send(i + "");

            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}

```

## OkHttp-websocket

```java
package com.cvte.rt.wsclient;

import okhttp3.*;
import okio.ByteString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述: WebSocket客户端，代替HTML中的请求实现
 * 由okhttp组件提供
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
public class MyOkHttpWebSocketClient extends WebSocketListener {
    private Long total = 0L;
    private Long succeed = 0L;
    private Long failed = 0L;

    private Logger logger = LoggerFactory.getLogger(MyOkHttpWebSocketClient.class);


    @Override
    public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        super.onClosed(webSocket, code, reason);
        logger.info("【okHttp】onClosed>>>关闭状态码：" + code + ",关闭原因：" + reason);
    }

    @Override
    public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        super.onClosing(webSocket, code, reason);
        logger.info("【okHttp】onClosing>>>关闭状态码：" + code + ",关闭原因：" + reason);

    }

    @Override
    public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable Response response) {
        super.onFailure(webSocket, t, response);
        logger.info("【okHttp】onFailure>>>异常原因：" + t.getMessage() + ",响应内容：" + response);

    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
        super.onMessage(webSocket, text);
        logger.info("【okHttp】onMessage>>>接收(文本)消息：" + text );
        String msg = "用户:发送命令:" + total;
        textSucceedRate(text,msg);
    }

    public void textSucceedRate(String origin, String target) {
        if (origin.equals(target)) {
            succeed++;
        } else {
            failed++;
        }
        total++;
        logger.info(toString());
    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull ByteString bytes) {
        super.onMessage(webSocket, bytes);
        logger.info("【okHttp】onMessage>>>接收(字节)消息：" + bytes );
    }

    @Override
    public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
        super.onOpen(webSocket, response);
        logger.info("【okHttp】onOpen>>>打开连接响应内容：" + response );

    }
    @Override
    public String toString() {
        return "MyOkHttpWebSocketClient{总请求数："+total
                +" 成功数量："+succeed
                +" 失败数量："+failed
                +"}";
    }
    public static void main(String[] args) {
        OkHttpClient webSocketClient = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder().url("ws://localhost:8080/myHandler").build();
        WebSocket webSocket1 = webSocketClient.newWebSocket(request, new MyOkHttpWebSocketClient());
        MyOkHttpWebSocketClient.send(webSocket1);
        //仅仅是Client端的关闭，
        webSocket1.close(1000,"手动关闭");
        webSocket1.send("a");
        WebSocket webSocket2 = webSocketClient.newWebSocket(request, new MyOkHttpWebSocketClient());
        MyOkHttpWebSocketClient.send(webSocket2);
        //仅仅是Client端的关闭，
        webSocket2.close(1000,"手动关闭");
        webSocket2.send("b");
    }

    static void send(WebSocket webSocket) {

        for (int i = 0; i < 10000; i++) {
            webSocket.send(i + "");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

```



> 注意：不论是java-websocket还是OkHttp-websocket的实现方式，其消息都是靠websocket服务站点中的消息处理接收的。应该将响应的逻辑处理放在消息处理中。