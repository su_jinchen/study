# Spring_WebSocket

​	Spring Framework 4 包括一个新的`spring-websocket`模块，具有全面的 WebSocket 支持。WebSocket 协议[RFC 6455](https://tools.ietf.org/html/rfc6455)为 Web 应用程序定义了一项重要的新功能：Client 端与服务器之间的全双工双向通信。

> 最适合 WebSocket 的 Web 应用程序是 Client 端和服务器需要高频且低延迟地交换事件的 Web 应用程序。主要候选人包括但不限于金融，游戏，协作等领域的应用程序。这样的应用程序对时间延迟非常敏感，并且还需要高频交换各种消息。

​	Spring Framework 允许`@Controller`和`@RestController`类同时具有 HTTP 请求处理和 WebSocket 消息处理方法。此外，Spring MVC 请求处理方法或任何与此相关的应用程序方法都可以轻松地向所有感兴趣的 WebSocketClient 端或特定用户 Broadcast 消息。

## WebSocket-服务端实现方式

​	网上流传的几种WebSocket实现方式。

### 	方式一：javax包实现

#### 自定义WebSocket服务端点设置

```java
package com.xiaopangzi.websocket.directive.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * 描述: Spring集成原生WebSocketConfig配置
 * 作用：自定义WebSokcet服务端点配置
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
@Configuration
public class JavaxWebSocketConfig {
    /**
     * 创建服务器端点
     * @return
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}

```

> 有了这个Bean就可以使用@ServerEndpoint定义一个端点服务类。在这个端点服务类中可以定义websocket的打开、关闭、错误、和发送消息的方法。

#### 定义websocket服务端站点

```java
package com.xiaopangzi.websocket.directive.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 描述: WebSocket连接服务
 * 由Javax包提供支持
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
@ServerEndpoint("/ws")
@Service
public class MyJavaxWebSocketImpl {
    private Logger log = LoggerFactory.getLogger(MyJavaxWebSocketImpl.class);

    /**
     * 记录当前在线连接数，设计为线程安全的
     */
    private static int onlineCount = 0;

    /**
     * concurrent包是线程安全的Set，用来存放每个客户端对应的WebSockets
     * 每个客户端打开时，都会为其创建一个MyJavaxWebSocketImpl对象，
     */
    private static CopyOnWriteArraySet<MyJavaxWebSocketImpl> webSockets = new CopyOnWriteArraySet<MyJavaxWebSocketImpl>();

    /**
     * 当前正在使用的Session
     */
    private Session currentSession;


    /**
     * 建立连接，将在线客户端以sessionId保存
     *
     * @param session 会话
     */

    @OnOpen
    public void onOpen(Session session) {
        String id = session.getId();
        log.info("有新的客户端连接了: {}", id);
        this.currentSession = session;
        webSockets.add(this);
        sendOne("有新的连接加入",false);
    }

    /**
     * 关闭连接，移除在线客户端
     *
     * @param session
     */
    @OnClose
    public void onClose(Session session) {
        log.info("断开连接: {}", session.getId());
        webSockets.remove(this);
    }

    /**
     * 发生错误
     *
     * @param throwable 错误内容
     */
    @OnError
    public void onError(Throwable throwable,Session session) {
        log.error("有客户端连接发生错误！错误连接：{}，错误内容：{}", this.currentSession, throwable.getMessage());
        throwable.printStackTrace();
    }

    /**
     * 接收到消息，可对消息进行处理。
     *
     * @param message
     */
    @OnMessage
    public void onMessage(String message) {
        log.info("服务端接收到客户端的消息为：{}", message);
        if (message.equals("a")) {
            message = String.format("hellow {}%s", message);
            sendAll(message, false);
        } else {
            sendAll(message,false);
        }
    }


    /**
     * 服务器群发通知客户端
     * @param mesage
     */
    public void sendAll(String mesage,boolean async) {
            if (async) {
                for (MyJavaxWebSocketImpl websocket:webSockets
                     ) {
                    websocket.currentSession.getAsyncRemote().sendText(mesage);
                }
            } else {
                for (MyJavaxWebSocketImpl websocket:webSockets
                ) {
                    try {
                        websocket.currentSession.getBasicRemote().sendText(mesage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

    }

    /**
     * 服务器单独通知客户端
     * @param message
     */
    public void sendOne( String message,boolean async) {
            if (async) {
                this.currentSession.getAsyncRemote().sendText(message);

            } else {
                try {
                    this.currentSession.getBasicRemote().sendText(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
    }

}

```

> @ServerEndpoint("/ws")：让spring创建Websocket的服务端点，其中请求地址时ws
>
> @OnOpen：标注客户端打开websocket服务端站点的方法
>
> @OnClose：标注客户端关闭websocket服务端站点的方法
>
> @OnError：标注客户端请求websocket服务端站点发生异常的方法
>
> @OnMessage：标注客户端发送消息，websocket服务端站点调用方法

#### 测试代码：

```html
<!DOCTYPE HTML>
<html>
<head>
    <title>Test My WebSocket</title>
</head>


<body>
TestWebSocket
<input  id="text" type="text" />
<button onclick="send()">SEND MESSAGE</button>
<button onclick="closeWebSocket()">CLOSE</button>
<div id="message"></div>
</body>

<script type="text/javascript">
    var websocket = null;


    //判断当前浏览器是否支持WebSocket
    if('WebSocket' in window){
        //连接WebSocket节点
        websocket = new WebSocket("ws://localhost:8080/ws");
    }
    else{
        alert('Not support websocket')
    }


    //连接发生错误的回调方法
    websocket.onerror = function(){
        setMessageInnerHTML("error");
    };


    //连接成功建立的回调方法
    websocket.onopen = function(event){
        setMessageInnerHTML("open");
    }


    //接收到消息的回调方法
    websocket.onmessage = function(event){
        setMessageInnerHTML(event.data);
    }


    //连接关闭的回调方法
    websocket.onclose = function(){
        setMessageInnerHTML("close");
    }


    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function(){
        websocket.close();
    }


    //将消息显示在网页上
    function setMessageInnerHTML(innerHTML){
        document.getElementById('message').innerHTML += innerHTML + '<br/>';
    }


    //关闭连接
    function closeWebSocket(){
        websocket.close();
    }


    //发送消息
    function send(){
        var message = document.getElementById('text').value;
        websocket.send(message);
    }
</script>
</html>
```

### 方式二：Spring对Websocket的支持

> Spring Framework 4 包括一个新的`spring-websocket`模块，具有全面的 WebSocket 支持。它与 Java WebSocket API 标准([JSR-356](https://jcp.org/en/jsr/detail?id=356))兼容，并且还提供了附加的附加值，如引言的其余部分所述。

​	与 HTTP(它是应用程序级协议)不同，在 WebSocket 协议中，传入消息中根本没有足够的信息供框架或容器知道如何路由或处理它。因此，对于非常琐碎的应用程序而言，WebSocket 的级别可以说太低了。可以做到，但是可能会导致在顶部创建一个框架。这相当于当今大多数 Web 应用程序是使用 Web 框架而不是仅使用 Servlet API 编写的。

​	它是低延迟和高频率消息的结合，这使得使用 WebSocket 协议变得至关重要。即使在这样的应用程序中，仍然选择是否所有 Client 端-服务器通信都应该通过 WebSocket 消息完成，而不是使用 HTTP 和 REST。答案将因应用程序而异。但是，某些功能可能同时通过 WebSocket 和 REST API 公开，以便为 Client 端提供替代方案。此外，REST API 调用可能需要向通过 WebSocket 连接的感兴趣的 Client 端 Broadcast 消息。

#### 自定义websocket服务端站点

```java
package com.xiaopangzi.websocket.directive.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.*;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
public class MyWebSocketHandler implements WebSocketHandler {
    private Logger logger = LoggerFactory.getLogger(MyWebSocketHandler.class);
    private static ConcurrentHashMap<String, WebSocketSession> clients = new ConcurrentHashMap<String, WebSocketSession>();
    /**
     * 连接成功后
     * @param session
     * @throws Exception
     */
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String id = session.getId();
        clients.put(id, session);
        logger.info("{}已建立连接，clients数量:{}",id,clients.size());
    }

    /**
     * 获取请求消息
     * @param session
     * @param message
     * @throws Exception
     */
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        Object payload = message.getPayload();
        System.out.println(payload);
        String id = session.getId();
        logger.info("{}获取消息，消息内容:{} >>>当前连接数量：{}", id, message.toString(), clients.size());
//        session.sendMessage(message);
        sendAll(message);
    }

    /**
     * 消息处理异常
     * @param session
     * @param exception
     * @throws Exception
     */
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        String id = session.getId();
        logger.info("{}消息处理异常，消息内容:{} >>>当前连接数量：{}",id,exception.getMessage(),clients.size());
    }

    /**
     * 关闭连接
     * @param session
     * @param closeStatus
     * @throws Exception
     */
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        String id = session.getId();
        logger.info("{}连接关闭，关闭信息:{} >>>当前连接数量：{}", id, closeStatus.toString(), clients.size());
        clients.remove(session.getId());
    }

    /**
     * 支持分块消息，默认即可
     * @return
     */
    public boolean supportsPartialMessages() {
        return false;
    }

    /**
     * 消息群发
     * @param message
     * @return
     * @throws IOException
     */
    public Object sendAll(Object message) throws IOException {
        TextMessage textMessage = null;
        if (message instanceof TextMessage) {
            textMessage = (TextMessage) message;
        } else {
            textMessage=new TextMessage(new StringBuffer(message.toString()));
        }
        for (Map.Entry<String, WebSocketSession> sessionEntry :
                clients.entrySet()) {
            WebSocketSession value = sessionEntry.getValue();
            String key = sessionEntry.getKey();
            value.sendMessage(textMessage);
            logger.info("用户{}为：【{}】接收消息：{}",key,value,message);

        }
        return true;
    }
}

```

#### 拦截器

```java
package com.xiaopangzi.websocket.directive.intercepter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;
import java.util.UUID;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
public class WebSocketInterceptor implements HandshakeInterceptor {
    private Logger logger = LoggerFactory.getLogger(WebSocketHandler.class);

    /**
     * webSocket握手的前置处理方法，
     * @param serverHttpRequest
     * @param response
     * @param wsHandler
     * @param attributes
     * @return
     * @throws Exception
     */
    public boolean beforeHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        if (serverHttpRequest instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest request = (ServletServerHttpRequest) serverHttpRequest;
            //生成一个UUID
            String uuid = UUID.randomUUID().toString().replace("-","");
            //将uuid放到websocketsession中
            //将uuid放到websocketsession中
            attributes.put("user_uuid", uuid);
            logger.info("beforeHandshake>>user_uuid：{}",uuid);
            return true;
        }
        return false;
    }

    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

    }
}

```

#### websocket配置类

```java
package com.xiaopangzi.websocket.directive.config;

import com.xiaopangzi.websocket.directive.handler.MyHandler;
import com.xiaopangzi.websocket.directive.handler.MyWebSocketHandler;
import com.xiaopangzi.websocket.directive.intercepter.WebSocketInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        webSocketHandlerRegistry
                .addHandler(myHandler(), "/myHandler")
                .addHandler(myWebSocketHandler(), "/myWebSocketHandler")
                .addInterceptors(new WebSocketInterceptor())
                .setAllowedOrigins("*");
    }

    @Bean
    public WebSocketHandler myHandler() {
        return new MyHandler();
    }

    @Bean
    public WebSocketHandler myWebSocketHandler() {
        return new MyWebSocketHandler();
    }
}

```

## WebSocket API

​	Spring 框架提供了一个 WebSocket API，旨在适应各种 WebSocket 引擎。当前，列表包括 WebSocket 运行时，例如 Tomcat 7.0.47，Jetty 9.1，GlassFish 4.1，WebLogic 12.1.3 和 Undertow 1.0(和 WildFly 8.0)。随着更多的 WebSocket 运行时可用，可能会添加其他支持。

​	对于应用程序而言，直接使用 WebSocket API 的级别太低-除非假设消息的格式，否则几乎没有框架可以解释消息或通过注解路由消息。这就是为什么应用程序应该考虑使用子协议和 Spring 的[通过 WebSocket 进行 STOMP](https://www.docs4dev.com/docs/zh/spring-framework/4.3.21.RELEASE/reference/websocket.html#websocket-stomp)支持的原因。

### WebSocketHandler

​	WebSocketHandler的作用是WebSocket消息和生命周期事件的处理程序，与javax包中使用@OnOpen注解标注的实现方式类似。

```java
package org.springframework.web.socket;

public interface WebSocketHandler {

	/**
	 * 在WebSocket协商成功并且WebSocket连接已打开并准备使用后调用
	 */
	void afterConnectionEstablished(WebSocketSession session) throws Exception;

	/**
	 * 当新的WebSocket消息到达时调用。
	 */
	void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception;

	/**
	 * 处理来自基础WebSocket消息传输的错误。
	 */
	void handleTransportError(WebSocketSession session, Throwable exception) throws Exception;

	/**
	 * 在任一侧关闭WebSocket连接之后或发生*传输错误之后调用。
	 * 尽管从技术上来说，会话可能仍处于打开状态，但是取决于底层实现，
	 * 因此此时不鼓励发送消息，并且很可能不会成功。
	 */
	void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception;

	/**
	 * WebSocketHandler是否处理部分消息。
	 * 如果此标志设置为 {@code true}并且基础的WebSocket服务器支持部分消息，
	 * 则大的WebSocket消息或大小未知的消息可能会被拆分，并且可能通过多次调用接收到。
	 */
	boolean supportsPartialMessages();

}

```

​	可以直接实现WebSocketHandler来定义一个服务端站点。不过Spring还提供了两种更方便的实现（BinaryWebSocketHandler和TextWebSocketHandler）。

> WebSocket 确实暗示着*消息传递体系结构*，但并不要求使用任何特定的*消息传递协议*。它是 TCP 上的一个非常薄的层，它将字节流转换为消息流(文本或二进制)，而没有更多。由应用程序来解释消息的含义。

#### TextWebSocketHandler

```java
public class TextWebSocketHandler extends AbstractWebSocketHandler {

	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
		try {
			session.close(CloseStatus.NOT_ACCEPTABLE.withReason("Binary messages not supported"));
		}
		catch (IOException ex) {
			// ignore
		}
	}

}
```

#### 自定义websocket服务站点

​	使用spring提供的websocket自定义一个websocket站点大致分为两步：

> 1. 实现WebSocketHandler接口或其子接口
> 2. websocket配置类中注册自定已的服务站点

1.实现WebSocketHandler子接口

​	这里举个实现WebSocketHandler子接口TextWebSocketHandler接口的例子。

```java
/**
 * 描述: 自定义的TextWebSocketHandler
 *
 * @author : sujinchen
 * @date : 2020/9/22
 */
public class MyTextWebSocketHandler extends TextWebSocketHandler {
}
```

> 这里不要求你必须去实现服务端点的打开、关闭、异常、接收消息等方法，因为有一套默认的实现。不过正常情况下都会使用到，所以也可以自己去实现一套适合自己使用的作为默认实现。

2.实现WebSocketConfigurer的服务配置

```java
package com.xiaopangzi.websocket.directive.config;

import com.xiaopangzi.websocket.directive.handler.MyTextWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * 描述: WebSocket服务的配置类
 *
 * @author : sujinchen
 * @date : 2020/9/22
 */
@Configuration
/**
 * 允许WebSocket使用
 */
@EnableWebSocket
public class MyWebSocketConfig implements WebSocketConfigurer {
    /**
     * 配置MyTextWebSocketHandler
     * @return
     */
    @Bean
    public MyTextWebSocketHandler myTextWebSocketHandler() {
        return new MyTextWebSocketHandler();
    }


    /**
     * 注册WebSocketHandlers的配置
     * @param registry
     */
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        //将自定义的服务站点映射到/myTextWs地址
        registry.addHandler(myTextWebSocketHandler(), "/myTextWs");
    }
}

```

### WebSocket握手

​	定制初始 HTTP WebSocket 握手请求的最简单方法是通过`HandshakeInterceptor`，该公开暴露握手方法的“之前”和“之后”。此类拦截器可用于排除握手或使`WebSocketSession`可以使用任何属性。例如，有一个内置的拦截器，用于将 HTTP 会话属性传递到 WebSocket 会话：

```java
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new MyHandler(), "/myHandler")
            .addInterceptors(new HttpSessionHandshakeInterceptor());
    }

}
```

HandshakeInterceptor

```java


package org.springframework.web.socket.server;

import java.util.Map;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.web.socket.WebSocketHandler;

/**
 * 用于WebSocket握手请求的拦截器。可用于检查握手请求和响应，以及将属性传递给目标
 */
public interface HandshakeInterceptor {

	/**
	 * 在处理握手之前调用。
	 */
	boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
			WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception;

	/**
	 * 握手完成后调用。响应状态和标题指示
	 */
	void afterHandshake(ServerHttpRequest request, ServerHttpResponse response,
			WebSocketHandler wsHandler, @Nullable Exception exception);

}

```

### WebSocketHandler 装饰

​	Spring 提供了一个`WebSocketHandlerDecorator`Base Class，可用于装饰`WebSocketHandler`并具有其他行为。使用 WebSocket Java 配置或 XML 名称空间时，默认情况下会提供并添加日志记录和异常处理实现。 `ExceptionWebSocketHandlerDecorator`捕获由任何 WebSocketHandler 方法引起的所有未捕获的异常，并关闭状态为`1011`的 WebSocket 会话，该会话指示服务器错误

### 配置WebSocket引擎

​	每个基础 WebSocket 引擎都公开配置属性，这些属性控制运行时特性，例如消息缓冲区大小的大小，空闲超时等。

```java
    /**
     * 将ServletServerContainerFactoryBean添加到 WebSocket Java 配置中
     * @return
     */
    @Bean
    public ServletServerContainerFactoryBean createWebSocketContainer() {
        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        container.setMaxTextMessageBufferSize(8192);
        container.setMaxBinaryMessageBufferSize(8192);
        return container;
    }
```

​	以上操作等同于使用javax包中的WebSocketContainer

```java
WebSocketContainer webSocketContainer = ContainerProvider.getWebSocketContainer();
```

​	对于其他的方式，比如Jetty，需要提供一个预先配置的Jetty的WebSocketFactory，然后通过WebSocket Java配置将其插入Spring的DefaulHandshakeHandler中。

```java
    @Bean
    public DefaultHandshakeHandler handshakeHandler() {

        WebSocketPolicy policy = new WebSocketPolicy(WebSocketBehavior.SERVER);
        policy.setInputBufferSize(8192);
        policy.setIdleTimeout(600000);

        return new DefaultHandshakeHandler(
                new JettyRequestUpgradeStrategy(new WebSocketServerFactory(policy)));
    }
```





### 	配置允许的来源

​		从 Spring Framework 4.1.5 开始，WebSocket 和 SockJS 的默认行为是仅接受“相同来源”请求。也可以允许* all *或指定的来源列表。此检查主要用于浏览器 Client 端。一般来说来源分为以下三种可能的行为：

> - **仅允许相同的原始请求(默认)**：在此模式下，启用 SockJS 时，Iframe HTTP 响应 Headers`X-Frame-Options`设置为`SAMEORIGIN`，并且 JSONP 传输被禁用，因为它不允许检查请求的来源。因此，启用此模式时，不支持 IE6 和 IE7.
> - **允许指定来源列表**：每个提供的*允许的来源*必须以`http://`或`https://`开头。在此模式下，启用 SockJS 时，将同时禁用基于 IFrame 和 JSONP 的传输。因此，启用此模式时，不支持 IE6 至 IE9.
> - **允许所有来源**：要启用此模式，您应提供`*`作为允许的来源值。在这种模式下，所有传输都可用。

```java
    /**
     * 注册WebSocketHandlers的配置
     * @param registry
     */
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        //将自定义的服务站点映射到/myTextWs地址
        registry.addHandler(myTextWebSocketHandler(), "/myTextWs")
                //添加握手拦截器
                .addInterceptors(new MyHandshakeInterceptor())
                //配置允许的来源
                .setAllowedOrigins("http://www.baidu.com","http://localhost:8080","http://localhost:63342");
    }
```

> 一般来说，可以通过setAllowedOrigins()方法来拦截当前WebSocket发起请求的位置，这样可以避免不法份子获取到WebSocket请求连接后，从任意位置发起请求攻击。当然了setAllowedOrigins()是在HandshakeInterceptor执行后才执行的，也可以自己在握手环节实现拦截输入源的操作。例子如下：

```java
    /**
     * 在处理握手之前调用。
     */
    public boolean beforeHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse response,
                            WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception{
        if (serverHttpRequest instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest request = (ServletServerHttpRequest) serverHttpRequest;
            //生成一个UUID
            String uuid = UUID.randomUUID().toString().replace("-","");
            //将uuid放到websocketsession中
            //将uuid放到websocketsession中
            attributes.put("user_uuid", uuid);
            logger.info("beforeHandshake>>user_uuid：{}",uuid);
            HttpHeaders headers = serverHttpRequest.getHeaders();
            String origin = headers.getOrigin();
            logger.info("origin>>>"+origin);
            if (origin.equals("http://localhost:63342")) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
```

> 注意：因为setAllowedOrigins()方法在握手拦截器执行完毕后执行，所以在握手拦截器中配置了就不要再在setAllowedOrigins()方法中配置，后者会覆盖前者。

SockJS后备

> ​	SockJS 的目标是让应用程序使用 WebSocket API，如果在运行时有必要，可以使用非 WebSocket 替代方案，且无需更改应用程序代码。

​	在不支持WebSocket的情况下，也可以很简单地实现WebSocket的功能的，方法就是使用 [SockJS](https://github.com/sockjs/sockjs-client)。它会优先选择WebSocket进行连接，但是当服务器或客户端不支持WebSocket时，会自动在 XHR流、XDR流、iFrame事件源、iFrame HTML文件、XHR轮询、XDR轮询、iFrame XHR轮询、JSONP轮询 这几个方案中择优进行连接。

此处不做叙述！