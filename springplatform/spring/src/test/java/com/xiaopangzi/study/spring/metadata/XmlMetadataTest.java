package com.xiaopangzi.study.spring.metadata;


import com.xiaopangzi.study.spring.config.MetadataConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/12
 */
class XmlMetadataTest {

    @Test
    public void test () {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring.xml");
        XmlMetadata bean = (XmlMetadata) context.getBean("xmlMetadata");
        System.out.println(bean);

    }

    public static void main(String[] args) {
        //从xml文件中加载资源到容器中
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
//        从容器中获取Bean
//        XmlMetadata bean =  context.getBean("xmlMetadata",XmlMetadata.class);
//        System.out.println(bean);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MetadataConfig.class);
        XmlMetadata xmlMetadata = context.getBean("xmlMetadataJavaAnnotation", XmlMetadata.class);
        System.out.println(xmlMetadata);
    }

    @Test
    public void testMetadataConfig() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MetadataConfig.class);
        XmlMetadata xmlMetadata = context.getBean("xmlMetadata2", XmlMetadata.class);
        System.out.println(xmlMetadata);
    }
}