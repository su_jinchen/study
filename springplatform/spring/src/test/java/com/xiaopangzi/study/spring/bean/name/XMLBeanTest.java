package com.xiaopangzi.study.spring.bean.name;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
class XMLBeanTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("xmlbean.xml");
        XMLBean xmlBean = context.getBean("xmlBean", XMLBean.class);
        System.out.println(xmlBean);
    }
}