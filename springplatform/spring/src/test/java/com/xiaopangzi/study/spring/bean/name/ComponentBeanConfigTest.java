package com.xiaopangzi.study.spring.bean.name;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
class ComponentBeanConfigTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ComponentBeanConfig.class);
        ComponentBean myComponentBean = context.getBean("myComponentBean", ComponentBean.class);
        System.out.println(myComponentBean);
    }
}