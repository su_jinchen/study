package com.xiaopangzi.study.spring.bean.customer;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
class CustomerBeanConfigTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(CustomerBeanConfig.class);
        CustomerBean customerBean = context.getBean("customerBean", CustomerBean.class);
        customerBean.start();

    }

}