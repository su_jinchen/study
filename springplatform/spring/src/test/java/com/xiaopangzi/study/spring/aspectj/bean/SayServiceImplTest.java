package com.xiaopangzi.study.spring.aspectj.bean;

import com.xiaopangzi.study.spring.aspectj.config.AspectJConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
class SayServiceImplTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AspectJConfig.class);
        TestBean sayService = context.getBean("testBean", TestBean.class);
        sayService.doSay();
    }
}