package com.xiaopangzi.study.spring.aop;

import com.xiaopangzi.study.spring.aop.impl.AopInterceptorImpl;
import com.xiaopangzi.study.spring.aop.impl.HellowServiceImpl;


/**
 * 描述: 测试类
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
class ProxyBeanTest {
    public static void main(String[] args) {
        //生成目标对象
        HellowService hellowService = new HellowServiceImpl();
        //绑定目标对象和拦截器
        ProxyBean proxyBean = new ProxyBean(new AopInterceptorImpl(), hellowService);
        //生成代理对象
        HellowService proxy = (HellowService) proxyBean.getProxyBean();
        //代理对象调用方法
        proxy.say();
    }

}