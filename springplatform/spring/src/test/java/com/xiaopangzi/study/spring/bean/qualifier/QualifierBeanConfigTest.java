package com.xiaopangzi.study.spring.bean.qualifier;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
class QualifierBeanConfigTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(QualifierBeanConfig.class);
        BeanDi beanDi = context.getBean("beanDi", BeanDi.class);
        beanDi.show();
    }

}