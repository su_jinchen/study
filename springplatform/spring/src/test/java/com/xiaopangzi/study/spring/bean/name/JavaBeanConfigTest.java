package com.xiaopangzi.study.spring.bean.name;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
class JavaBeanConfigTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JavaBeanConfig.class);
        JavaBean javaCodeBean = context.getBean("javaCodeBean", JavaBean.class);
        System.out.println(javaCodeBean);
    }

}