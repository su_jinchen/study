package com.xiaopangzi.study.spring.bean.di;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
class BeansConfigTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeansConfig.class);
        GetBeans beanConfig = context.getBean("getBeans", GetBeans.class);
        beanConfig.showBeans();

    }

}