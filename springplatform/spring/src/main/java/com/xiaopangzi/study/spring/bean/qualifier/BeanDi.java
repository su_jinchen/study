package com.xiaopangzi.study.spring.bean.qualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
@Component
public class BeanDi {
    @Autowired
    @MyQualifier(value = "qualifierBean",used = "QualifierBean")
    QualifierBean qualifierBean;

    public void show() {
        System.out.println(qualifierBean.getName());
    }
}
