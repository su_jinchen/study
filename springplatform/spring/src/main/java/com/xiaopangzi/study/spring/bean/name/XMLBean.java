package com.xiaopangzi.study.spring.bean.name;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
public class XMLBean {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "XMLBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
