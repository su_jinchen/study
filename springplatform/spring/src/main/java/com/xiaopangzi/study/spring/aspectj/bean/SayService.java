package com.xiaopangzi.study.spring.aspectj.bean;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public interface SayService {
    void say(String a,String b);

    void sayB(String a, String b);
}
