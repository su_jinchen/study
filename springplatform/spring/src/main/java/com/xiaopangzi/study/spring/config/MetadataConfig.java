package com.xiaopangzi.study.spring.config;

import com.xiaopangzi.study.spring.metadata.XmlMetadata;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述: 基于java代码的配置
 *
 * @author : sujinchen
 * @date : 2020/9/13
 */
@Configuration
@ComponentScan(basePackageClasses = XmlMetadata.class)
public class MetadataConfig {
    @Bean(name = {"xmlMetadata2"})
    public XmlMetadata getMetadata() {
        XmlMetadata xmlMetadata = new XmlMetadata();
        xmlMetadata.setId(2L);
        xmlMetadata.setName("getMetadata");
        xmlMetadata.setDescribe("基于java代码的配置");
        return xmlMetadata;
    }
    @Bean(name = {"xmlMetadata3"})
    public XmlMetadata getMetadata2() {
        XmlMetadata xmlMetadata = new XmlMetadata();
        xmlMetadata.setId(2L);
        xmlMetadata.setName("getMetadata");
        xmlMetadata.setDescribe("基于java代码的配置");
        return xmlMetadata;
    }
}
