package com.xiaopangzi.study.spring.bean;

import com.xiaopangzi.study.spring.bean.name.XMLBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
@Component
public class GetApplicationContext {

    @Autowired
    public ApplicationContext getApplicationContext(ApplicationContext applicationContext) {
        return applicationContext;
    }

    public static void main(String[] args) {
    }
}
