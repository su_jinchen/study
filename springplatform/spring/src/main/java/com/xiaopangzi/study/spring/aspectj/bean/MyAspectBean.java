package com.xiaopangzi.study.spring.aspectj.bean;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * 描述: 声明一个切面，切面支持定义切点、建议等
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
@Aspect
public class MyAspectBean {
    @Pointcut("execution(* com.xiaopangzi.study.spring..*.say*(..))&&args(a,..)")
    public void sayPonitCut(String a) {
        System.out.println("sayPonitCut>>>开始执行切入点");
    }


    @Before("execution(* com.xiaopangzi.study.spring.aspectj.bean.SayServiceImpl.say())")
    public void before() {
        System.out.println("MyAspectBean>>>before");
    }

    @After("sayPonitCut(a)")
    public void after(String a) {
        System.out.println("MyAspectBean>>>after->"+a);
    }

//    @AfterReturning("sayPonitCut()")
    public void afterReturn() {
        System.out.println("MyAspectBean>>>afterReturn");

    }

//    @AfterThrowing("sayPonitCut()")
    public void afterThrowing() {
        System.out.println("MyAspectBean>>>afterThrowing");

    }

//    @Around("sayPonitCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("MyAspectBean>>>around.before");
        Object proceed = joinPoint.proceed();
        System.out.println("MyAspectBean>>>around.after");

        return proceed;
    }
}
