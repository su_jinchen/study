package com.xiaopangzi.study.spring.bean.instance;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Configuration
public class JavaInstanceFactoryBean {
    private static JavaStaticFactoryBean javaStaticFactoryBean = new JavaStaticFactoryBean();

    @Bean
    @Lazy
    public JavaStaticFactoryBean getJavaStaticFactoryBean() {
        return javaStaticFactoryBean;
    }
}
