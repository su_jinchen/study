package com.xiaopangzi.study.spring.aspectj.config;

import com.xiaopangzi.study.spring.aspectj.bean.MyAspectBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 描述: 通过 Java 配置启用@AspectJ 支持
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.xiaopangzi.study.spring.aspectj")
public class AspectJConfig {

    /**
     * 注入切面
     * @return
     */
    @Bean
    public MyAspectBean myAspectBean() {
        return new MyAspectBean();
    }
}
