package com.xiaopangzi.study.spring.bean.scope;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;

import java.util.Map;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
public class SingletonScopeBean implements ApplicationContextAware {
    private ApplicationContext context;
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    /**
     * 从容器中获取prototype类型的bean
     * @return
     */
    protected PrototypeScopeBean createPrototypeScopeBean() {
        return this.context.getBean("prototypeScopeBean", PrototypeScopeBean.class);
    }


    public void userPrototypeScopeBeanMethodOne() {
        //每次从中获取
        PrototypeScopeBean prototypeScopeBean = createPrototypeScopeBean();
    }
}
