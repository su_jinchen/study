package com.xiaopangzi.study.spring.aop;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 描述: 调用器类，通过反射执行target对应方法
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class Invocation {
    /**
     * 参数列表
     */
    private Object[] params;
    /**
     * 反射执行方法
     */
    private Method method;
    /**
     * 传入的目标对象
     */
    private Object target;



    /**
     * 构造方法
     *
     * @param params 方法参数列表
     * @param method 单个方法
     * @param target 目标对象
     */
    public Invocation(Object[] params, Method method, Object target) {
        this.params = params;
        this.method = method;
        this.target = target;
    }

    /**
     * 通过反射调用目标对象的方法执行
     * @return object 目标对象调用结果
     * @throws InvocationTargetException 目标对象调用异常
     * @throws IllegalAccessException 非法存取异常
     */
    public Object proceed() throws InvocationTargetException, IllegalAccessException {
            return method.invoke(target, params);
    }


    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }
}
