package com.xiaopangzi.study.spring.aspectj.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
@Service
public class TestBean {
    @Autowired
    SayService sayService;

    public void doSay() {
        sayService.say("哼！","嗨");
    }

}
