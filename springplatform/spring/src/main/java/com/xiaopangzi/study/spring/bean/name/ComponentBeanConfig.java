package com.xiaopangzi.study.spring.bean.name;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Configuration
@ComponentScan(basePackageClasses = ComponentBean.class)
public class ComponentBeanConfig {
}
