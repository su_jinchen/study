package com.xiaopangzi.study.spring.bean.instance;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
public class StaticFactoryBean {
    private static StaticFactoryBean staticFactoryBean = new StaticFactoryBean();

    public static StaticFactoryBean create() {
        return staticFactoryBean;
    }
}
