package com.xiaopangzi.study.spring.bean.scope;

import org.springframework.context.annotation.Scope;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Scope("prototype")
public class PrototypeScopeBean {
}
