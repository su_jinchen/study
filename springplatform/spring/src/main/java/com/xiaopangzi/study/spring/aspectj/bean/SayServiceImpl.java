package com.xiaopangzi.study.spring.aspectj.bean;

import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
@Component
public class SayServiceImpl implements SayService {
    @Override
    public void say(String a,String b) {
        System.out.println("你好！");
        sayB("B>>"+a,"B>>"+b);
    }

    @Override
    public void sayB(String a, String b) {
        System.out.println("你好B！");
    }
}
