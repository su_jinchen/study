package com.xiaopangzi.study.spring.bean.di;

import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
@Component
public class AdminBean implements Beans {
    @Override
    public String getBeanName() {
        return "AdminBean";
    }
}
