package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象的经纪人
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class ProxyBean extends TargetBean {
    /**
     * 经纪人的科学败家
     */
    @Override
    public Object spendMoney() {
        //前置增强
        System.out.println("before:我是经纪人!");
        //保存目标对象方法执行结果
        Object result = null;
        //异常标志位
        boolean exceptionFlag = false;
        try {
            //目标对象方法调用
            result = super.spendMoney();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            exceptionFlag = true;
        }
        if (exceptionFlag) {
            //抛出问题增强
            System.out.println("afterThrowing:老板破产了！");
        }
        //返回增强
        if (result == null || result.equals("")) {
            System.out.println("afterReturn:老板吹牛呢，不打钱败什么家！");
        }
        //后置增强
        System.out.println("after:我来帮助老板科学败家！");
        return result;
    }



    @Override
    public Object getInfo(ProxyBean proxyBean) {
        System.out.println("before：获取信息！");
        return super.getInfo(proxyBean);
    }
    public static void main(String[] args) {
        ProxyBean proxyBean = new ProxyBean();
        proxyBean.getInfo(proxyBean);
    }
}
