package com.xiaopangzi.study.spring.bean.qualifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
@Configuration
@ComponentScan(basePackages = "com.xiaopangzi.study.spring.bean.qualifier")
public class QualifierBeanConfig {

}
