package com.xiaopangzi.study.spring.bean.di.way;

import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Configuration
public class BeanResourceDI {
    @Resource
    private BeanResource beanResource;
    public BeanResourceDI() {
    }
}
