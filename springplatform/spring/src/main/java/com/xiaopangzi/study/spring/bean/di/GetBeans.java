package com.xiaopangzi.study.spring.bean.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
@Component
public class GetBeans {
    @Autowired
    Beans[] beans;

    public void showBeans() {
        if (beans != null) {
            for (Beans bean:
                    beans) {
                System.out.println(bean.getBeanName());
            }
        }
    }
}
