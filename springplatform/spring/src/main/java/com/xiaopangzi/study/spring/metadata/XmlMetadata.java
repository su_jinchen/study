package com.xiaopangzi.study.spring.metadata;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 描述: 基于XML配置的bean
 *
 * @author : sujinchen
 * @date : 2020/9/12
 */
@Component("xmlMetadataJavaAnnotation")
public class XmlMetadata {
    /**
     * id:标识号
     */
//    @Value("3")
    private Long id;
    /**
     * name:名称
     */
//    @Value("xmlMetadataJavaAnnotation")
    private String name;
    /**
     * describe：描述
     */
//    @Value("基于java注解的配置")
    private String describe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "XmlMetadata{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }
}
