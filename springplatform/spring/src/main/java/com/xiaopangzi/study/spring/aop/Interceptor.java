package com.xiaopangzi.study.spring.aop;

import java.lang.reflect.InvocationTargetException;

/**
 * 描述: 规则接口，作为自定义约定规则使用
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public interface Interceptor {
    /**
     * 所有事件执行前执行
     */
    void before();

    /**
     * 所有事件执行后执行
     */
    void after();


    /**
     * 是否执行around方法，为默认方法，默认不执行。
     * @return
     */
    default boolean useAround(){
        return false;
    }

    /**
     * 调用目标对象的方法,默认调用
     * @param invocation
     * @return 目标对象方法调用结果
     * @throws InvocationTargetException 目标对象调用异常
     * @throws IllegalAccessException 非法存取异常
     */
    default Object around(Invocation invocation) throws InvocationTargetException, IllegalAccessException{
        return invocation.proceed();
    }

    /**
     * 事件执行后，无异常执行此方法。
     */
    void afterReturn();

    /**
     * 事件执行后，出现异常执行此方法。
     */
    void afterThrowing();

}
