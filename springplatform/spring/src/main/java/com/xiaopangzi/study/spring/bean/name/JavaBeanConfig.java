package com.xiaopangzi.study.spring.bean.name;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Configuration
@ComponentScan(basePackageClasses = JavaBean.class)
public class JavaBeanConfig {
    @Bean(name = {"javaCodeBean"})
    public JavaBean javaBean() {
        JavaBean javaBean = new JavaBean();
        javaBean.setId(2L);
        javaBean.setName("基于java代码的配置Bean");
        return javaBean;
    }
}
