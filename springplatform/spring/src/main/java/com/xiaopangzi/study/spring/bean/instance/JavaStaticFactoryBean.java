package com.xiaopangzi.study.spring.bean.instance;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Configuration
public class JavaStaticFactoryBean {

    private static JavaStaticFactoryBean javaStaticFactoryBean = new JavaStaticFactoryBean();
    @Bean
    public static JavaStaticFactoryBean getInstance() {
        return javaStaticFactoryBean;
    }
}
