package com.xiaopangzi.study.spring.resource;

import org.springframework.core.io.UrlResource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/19
 */
public class UrlResourceDemo {
    public static void main(String[] args) {
        try {
            UrlResource urlResource = new UrlResource("https://www.cnblogs.com/ddwarehouse/p/10127729.html");
            String description = urlResource.getDescription();
            System.out.println(description);
            InputStream inputStream = urlResource.getInputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            while((len = inputStream.read(buffer)) != -1) {
                bos.write(buffer, 0, len);
            }
            bos.close();
            byte[] result = bos.toByteArray();
            String str = new String(result);
            System.out.println ("打印内容："+str);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
