package com.xiaopangzi.study.spring.bean.instance;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
public class InstanceFactoryBean {
    private InstanceFactoryBean instanceFactoryBean = new InstanceFactoryBean();
    public InstanceFactoryBean createOne() {
        return this.instanceFactoryBean;
    }
    public InstanceFactoryBean createTwo() {
        return new InstanceFactoryBean();
    }
}
