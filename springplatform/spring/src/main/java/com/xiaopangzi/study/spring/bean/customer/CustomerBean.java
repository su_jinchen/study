package com.xiaopangzi.study.spring.bean.customer;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.Lifecycle;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 描述: 自定义Bean
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
public class CustomerBean implements InitializingBean, DisposableBean, Lifecycle {
    /**
     * 初始化回调,由InitializingBean接口定义
     * 缺点：将不必要代码耦合到Spring
     * @throws Exception
     */
    public void afterPropertiesSet() throws Exception {
        System.out.println("CustomerBean_>>>>>>afterPropertiesSet()");
    }

    /**
     * 初始化回调,由@PostConstruct注解标注
     * 缺点：将不必要代码耦合到Spring
     */
    @PostConstruct
    public void customerPostConstruct() {
        System.out.println("CustomerBean_>>>>>>customerPostConstruct()");

    }

    /**
     * 初始化回调，由init_method方式定义
     * 优点：不会将代码耦合到 Spring
     */
    public void initMethod() {
        System.out.println("CustomerBean_>>>>>>initMethod()");
    }

    /**
     * 销毁回调,由@PreDestroy注解标注
     * 缺点：将不必要代码耦合到Spring
     */
    @PreDestroy
    public void customerPreDestroy() {
        System.out.println("CustomerBean_>>>>>>customerPreDestroy()");

    }
    /**
     * 销毁回调，由destroyMethod方式定义
     * 优点：不会将代码耦合到 Spring
     */
    public void destoryMethod() {
        System.out.println("CustomerBean_>>>>>>close()");
    }

    /**
     * 销毁回调,由DisposableBean接口定义
     * 缺点：将不必要代码耦合到Spring
     * @throws Exception
     */
    public void destroy() throws Exception {
        System.out.println("CustomerBean_>>>>>>destroy()");
    }

    public void start() {
        System.out.println("CustomerBean_>>>>>>start()");

    }

    public void stop() {
        System.out.println("CustomerBean_>>>>>>stop()");

    }

    public boolean isRunning() {
        return true;
    }
}
