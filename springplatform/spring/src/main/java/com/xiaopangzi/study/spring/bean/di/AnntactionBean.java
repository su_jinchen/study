package com.xiaopangzi.study.spring.bean.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Configuration
@ComponentScan(basePackageClasses = BeanC.class)
public class AnntactionBean {
    @Autowired
    private BeanC beanC;

    @Autowired
    public AnntactionBean(BeanC beanC) {
    }

    @Autowired
    public void setBeanC(BeanC beanC) {
        this.beanC = beanC;
    }
}

