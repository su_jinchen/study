package com.xiaopangzi.study.spring.aop.impl;

import com.xiaopangzi.study.spring.aop.Interceptor;
import com.xiaopangzi.study.spring.aop.Invocation;

import java.lang.reflect.InvocationTargetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述: AOP拦截器自定义实现，
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class AopInterceptorImpl implements Interceptor {

    @Override
    public void before() {
        System.out.println("before事件执行>>{}"+this.getClass().getName());
    }

    @Override
    public void after() {
        System.out.println("after>>{}"+this.getClass().getName());

    }

    @Override
    public boolean useAround() {
        System.out.println("after>>{}"+this.getClass().getName());
        return false;
    }

    @Override
    public Object around(Invocation invocation) throws InvocationTargetException, IllegalAccessException {
        System.out.println("around.before>>{}"+this.getClass().getName());
        Object proceed = null;
        invocation.proceed();
        System.out.println("around.after>>{}"+this.getClass().getName());
        return proceed;
    }

    @Override
    public void afterReturn() {
        System.out.println("afterReturn>>{}"+this.getClass().getName());

    }

    @Override
    public void afterThrowing() {
            System.out.println("afterThrowing>>{}"+this.getClass().getName());
    }
}
