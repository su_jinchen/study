package com.xiaopangzi.study.spring.aop.impl;

import com.xiaopangzi.study.spring.aop.HellowService;

/**
 * 描述: 目标对象接口
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public class HellowServiceImpl implements HellowService {
    @Override
    public void say() {
        System.out.println(" I am target!");
    }
}
