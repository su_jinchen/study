package com.xiaopangzi.study.spring.aopclass;

/**
 * 描述: 目标对象
 *
 * @author : sujinchen
 * @date : 2020/9/27
 */
public class TargetBean {
    /**
     * 目标对象的败家方法
     */
    public Object spendMoney () throws Throwable {
        saveMoney();
        System.out.println("target:千万富翁路，败家才能行！");
        return true;
    }

    /**
     * 目标对象的赚钱方法
     * @return
     */
    public Object saveMoney() {
        System.out.println("target:继承家产！");
        return true;
    }

    /**
     * 目标对象主动告诉经纪人该做啥
     * @return
     */
    public Object tellProxyDo(ProxyBean proxyBean) {
        System.out.println("target:经纪人你该做啥！");
        proxyBean.spendMoney();
        return true;
    }

    public Object getInfo(ProxyBean proxyBean) {
        return tellProxyDo(proxyBean);
    }
}
