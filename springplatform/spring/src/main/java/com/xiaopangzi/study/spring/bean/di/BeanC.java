package com.xiaopangzi.study.spring.bean.di;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Component
@Lazy
//@Scope("singleton")
@Scope("prototype")
public class BeanC {
}
