package com.xiaopangzi.study.spring.aop;

/**
 * 描述: 目标对象接口
 *
 * @author : sujinchen
 * @date : 2020/9/26
 */
public interface HellowService {
    void say();
}
