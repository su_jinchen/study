package com.xiaopangzi.study.spring.bean.qualifier;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
@Component
@MyQualifier(value = "qualifierBean",used = "QualifierBean")
public class QualifierBean {
    private String name = "注入";
    private String used = "注入";

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
