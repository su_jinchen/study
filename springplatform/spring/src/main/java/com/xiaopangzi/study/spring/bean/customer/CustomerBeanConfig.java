package com.xiaopangzi.study.spring.bean.customer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述: 配置类，CustomerBean
 *
 * @author : sujinchen
 * @date : 2020/9/17
 */
@Configuration
@ComponentScan(basePackageClasses = CustomerBean.class)
public class CustomerBeanConfig {

    @Bean(initMethod = "initMethod",destroyMethod = "destoryMethod")
    public CustomerBean customerBean() {
        return new CustomerBean();
    }
}
