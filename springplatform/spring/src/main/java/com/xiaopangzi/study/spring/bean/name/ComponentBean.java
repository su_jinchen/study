package com.xiaopangzi.study.spring.bean.name;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/16
 */
@Component("myComponentBean")
public class ComponentBean {

    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ComponentBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
