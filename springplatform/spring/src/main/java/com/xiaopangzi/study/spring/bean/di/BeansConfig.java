package com.xiaopangzi.study.spring.bean.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
@Configuration
@ComponentScan(basePackages = "com.xiaopangzi.study.spring.bean.di")
public class BeansConfig {
}
