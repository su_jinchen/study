package com.xiaopangzi.study.spring.bean.di;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/18
 */
public interface Beans {

    /**
     * 获取Bean名称
     * @return beanName
     */
    String getBeanName();
}
