package com.xiaopangzi.study.springboot.springapplication.exit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

/**
 * 描述: 自定义销毁Bean，可在Spring生命周期中进行回调
 *
 * @author : sujinchen
 * @date : 2020/9/11
 */
@Component
public class MyDisposableBean implements DisposableBean {
    Logger logger = LoggerFactory.getLogger(MyDisposableBean.class);

    @Override
    public void destroy() throws Exception {
        logger.info("MyDisposableBean.destory()>>销毁啦！");
    }

    @PreDestroy
    public void myPreDestory() {
        logger.info("@PreDestroy.myPreDestory()>>销毁啦！");

    }
}
