package com.xiaopangzi.study.springboot;

import com.xiaopangzi.study.springboot.exconfig.TestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 描述:SpringBoot默认启动类
 *
 * @author : sujinchen
 * @date : 2020/9/9
 */
@SpringBootApplication
public class MySpringBoot {

    @Autowired
    TestEntity entity;

    /**
     * 启动main方法
     *
     * @param args 命令行参数
     */
/*    public static void main(String[] args) {
        //传入启动类class
        SpringApplication.run(MySpringBoot.class, args);
    }*/


//    public static void main(String[] args) {
//        SpringApplication application=new SpringApplication(MySpringBoot.class);
//        //将Banner关闭
//        application.setBannerMode(Banner.Mode.CONSOLE);
//        //添加监听器
//        application.addListeners(new MyApplicationListener());
//        //设置Web环境类型,共计三种类型
//        application.setWebApplicationType(WebApplicationType.NONE);
//        application.setWebApplicationType(WebApplicationType.SERVLET);
//        application.setWebApplicationType(WebApplicationType.REACTIVE);
//        application.setApplicationContextClass(CustomizeApplicationContext.class);
//        SpringApplication.exit(application.run(args));
//    }



/*    public static void main(String[] args) {
        new SpringApplicationBuilder().sources(MySpringBoot.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }*/


    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(MySpringBoot.class);
        application.run(args);
    }

}
