package com.xiaopangzi.study.springboot.exconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;


/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/11
 */
@SpringBootTest
class TestEntityTest {

    @Autowired
    ApplicationContext applicationContext;
    @org.junit.Test
    public void test1() {

        TestEntity bean = applicationContext.getBean(TestEntity.class);
        System.out.println(bean);
    }
}