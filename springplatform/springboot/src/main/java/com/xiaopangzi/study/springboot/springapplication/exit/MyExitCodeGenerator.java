package com.xiaopangzi.study.springboot.springapplication.exit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;

/**
 * 描述: 在SpringApplication.exit()调用时返回特定的退出代码
 *
 * @author : sujinchen
 * @date : 2020/9/11
 */
@Component
public class MyExitCodeGenerator implements ExitCodeGenerator {
    Logger logger = LoggerFactory.getLogger(MyExitCodeGenerator.class);

    @Override
    public int getExitCode() {
        logger.info("MyExitCodeGenerator.getExitCode()>>1231231");
        return 1231231;
    }
}
