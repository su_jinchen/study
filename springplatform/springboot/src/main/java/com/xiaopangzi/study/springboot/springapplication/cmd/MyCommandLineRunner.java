package com.xiaopangzi.study.springboot.springapplication.cmd;

import com.xiaopangzi.study.springboot.springapplication.context.CustomizeApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 描述:CommandLineRunner的实现类，获取args参数进行处理
 *
 * @author : sujinchen
 * @date : 2020/9/10
 */
@Component
public class MyCommandLineRunner implements CommandLineRunner {
    Logger logger = LoggerFactory.getLogger(CustomizeApplicationContext.class);

    public void run(String... args) throws Exception {
        if (args == null) {
            logger.info("MyCommandLineRunner.run>>>args is null");
        } else {
            logger.info("MyCommandLineRunner.run>>>args is " + args);
        }
    }
}
