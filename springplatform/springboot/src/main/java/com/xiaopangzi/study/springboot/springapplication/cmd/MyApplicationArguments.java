package com.xiaopangzi.study.springboot.springapplication.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述: 提供对原始String[]参数的访问
 *
 * @author : sujinchen
 * @date : 2020/9/10
 */
@Component
public class MyApplicationArguments {
    Logger logger = LoggerFactory.getLogger(MyApplicationArguments.class);

    @Autowired
    public MyApplicationArguments(ApplicationArguments args) {
        boolean debug = args.containsOption("debug");
        List<String> files = args.getNonOptionArgs();
        logger.info("MyApplicationArguments.run>>>args is "+args);

    }
}
