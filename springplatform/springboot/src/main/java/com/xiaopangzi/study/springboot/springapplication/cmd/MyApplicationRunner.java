package com.xiaopangzi.study.springboot.springapplication.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 描述:ApplicationRunner的实现类，获取args参数进行处理
 *
 * @author : sujinchen
 * @date : 2020/9/10
 */
@Component
public class MyApplicationRunner implements ApplicationRunner {
    Logger logger = LoggerFactory.getLogger(MyApplicationRunner.class);

    public void run(ApplicationArguments args) throws Exception {
        if (args == null) {
            logger.info("MyApplicationRunner.run>>>args is null");
        } else {
            logger.info("MyApplicationRunner.run>>>args is " + args);
        }
    }
}
