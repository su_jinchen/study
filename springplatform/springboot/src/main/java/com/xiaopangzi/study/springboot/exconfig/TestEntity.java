package com.xiaopangzi.study.springboot.exconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 描述: 测试使用实体，用于获取配置文件中的值
 *
 * @author : sujinchen
 * @date : 2020/9/11
 */
@Component
public class TestEntity {
    /**
     * 序号
     */
    @Value("${entity.id}")
    private Integer id;
    /**
     * 姓名
     */
    @Value("${entity.name}")

    private String name;
    /**
     * 描述
     */
    @Value("${entity.describe}")

    private String describe;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }


}
