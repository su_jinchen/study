package com.xiaopangzi.study.springboot.springapplication.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.SpringApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * 描述: 自定义一个监听器，监听整个SpringApplicationEvent
 *
 * @author : sujinchen
 * @date : 2020/9/10
 */
public class MyApplicationListener implements ApplicationListener<SpringApplicationEvent> {
    private Logger logger = LoggerFactory.getLogger(MyApplicationListener.class);

    /**
     * s事件发生时就会自动监听，因为监听的时SpringApplicationEvent，所以其子类事件一并会被监听。
     * @param event
     */
    @Override
    public void onApplicationEvent(SpringApplicationEvent event) {
        logger.warn("MyApplicationListener event.getSource()>>>"+event.getSource().getClass());
    }
}
