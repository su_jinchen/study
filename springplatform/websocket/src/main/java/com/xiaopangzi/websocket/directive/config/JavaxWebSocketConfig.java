package com.xiaopangzi.websocket.directive.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * 描述: Spring集成原生WebSocketConfig配置
 * 作用：自定义WebSokcet服务端点配置
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
@Configuration
public class JavaxWebSocketConfig {
    /**
     * 创建服务器端点
     * @return
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
