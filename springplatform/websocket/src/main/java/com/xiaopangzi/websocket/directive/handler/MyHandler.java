package com.xiaopangzi.websocket.directive.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
public class MyHandler extends TextWebSocketHandler {
    private Logger logger = LoggerFactory.getLogger(MyHandler.class);

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.info(session.getHandshakeHeaders().toString());
        super.afterConnectionEstablished(session);
        logger.info("用户:{},连接WebSSH",session.getAttributes());
        String acceptedProtocol = session.getAcceptedProtocol();
        System.out.println(acceptedProtocol);
    }

    /**
     *
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        super.handleMessage(session, message);
        Object payload =  message.getPayload();
        String acceptedProtocol = session.getAcceptedProtocol();
        logger.info(acceptedProtocol);
        logger.info("用户:发送命令:{}",  message.toString());
        TextMessage textMessage = new TextMessage("用户:发送命令:"+payload);
        session.sendMessage(textMessage);
    }

}
