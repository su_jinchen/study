package com.xiaopangzi.websocket.directive.handler;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * 描述: 自定义的TextWebSocketHandler
 *
 * @author : sujinchen
 * @date : 2020/9/22
 */
public class MyTextWebSocketHandler extends TextWebSocketHandler {
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
    }
}
