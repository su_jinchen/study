package com.xiaopangzi.websocket.directive.message;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/21
 */
public class Command {
    /**
     * code：命令码
     */
    private Integer code;
    /**
     * command：命令
     */
    private String command;
    /**
     * message：消息描述
     */
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
