package com.xiaopangzi.stomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(com.xiaopangzi.websocket.App.class, args);
    }
}
