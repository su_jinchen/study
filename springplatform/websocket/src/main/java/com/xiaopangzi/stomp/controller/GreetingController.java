package com.xiaopangzi.stomp.controller;

import com.xiaopangzi.stomp.message.HellowGreeting;
import com.xiaopangzi.stomp.message.HellowMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

/**
 * 描述: 问候消息处理器
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
@Controller
@RequestMapping("/ws")
public class GreetingController {

    /**
     * 注入springBoot 自动配置消息模板对象
     */
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("/send")
    public Object send() {
        return "send";
    }
    @GetMapping("/receive")
    public Object receive() {
        return "receive";
    }
    @GetMapping("/sendUser")
    public Object sendUser() {
        return "sendUser";
    }


    //定义消息请求路径
    @MessageMapping("/send")
    //定义结果发送到特定路径
    @SendTo("/topic/greetings")
    public HellowGreeting greeting(HellowMessage message) throws Exception {
        //模拟延迟
        Thread.sleep(1000);
        return new HellowGreeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
}
