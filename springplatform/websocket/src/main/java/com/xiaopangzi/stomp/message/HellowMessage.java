package com.xiaopangzi.stomp.message;

/**
 * 描述: 消息请求内容体
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
public class HellowMessage {
    /**
     * name:姓名
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
