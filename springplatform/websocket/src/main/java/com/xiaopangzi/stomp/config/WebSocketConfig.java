package com.xiaopangzi.stomp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 描述:
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    /**
     * 注册stomp的服务端点
     *
     * @param registry
     */
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        /**
         * "/portfolio"是 WebSocket(或 SockJS)Client 端需要连接到的端点的 HTTP URL，以进行 WebSocket 握手。
         * 启用SockJS后备选项，以便在WebSocket不可用时可以使用备用传输。
         */
        registry.addEndpoint("/portfolio").setAllowedOrigins("*").withSockJS();
        registry.addEndpoint("/wsHello").setAllowedOrigins("*").withSockJS();
    }

    /**
     * 注册消息经纪人
     *
     * @param registry
     */
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        /**
         * 其目标 Headers 以"/app"开头的 STOMP 消息将路由到@Controller类中的@MessageMapping方法。
         */
        registry.setApplicationDestinationPrefixes("/app");
        /**
         * 使用内置的消息代理进行订阅和广播；将目标 Headers 以“/topic”或“/queue”开头的消息路由到代理。
         */
        registry.enableSimpleBroker("/topic", "/queue");
    }
}
