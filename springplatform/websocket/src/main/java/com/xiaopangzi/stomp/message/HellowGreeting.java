package com.xiaopangzi.stomp.message;

/**
 * 描述: 消息响应内容体
 *
 * @author : sujinchen
 * @date : 2020/9/24
 */
public class HellowGreeting {
    /**
     * content:问候内容
     */
    private String content;

    public HellowGreeting() {
    }

    public HellowGreeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
